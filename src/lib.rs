#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_migrations;
#[macro_use]
extern crate failure;
#[macro_use]
extern crate bitflags;
extern crate chrono;
extern crate feedly_api;
extern crate serde;
extern crate serde_json;
extern crate url;
#[macro_use]
extern crate serde_derive;
extern crate regex;
#[macro_use]
extern crate log;
extern crate atom_syndication;
extern crate base64;
extern crate image;
extern crate jsonfeed;
extern crate libxml;
extern crate mime_guess;
extern crate reqwest;
extern crate rss;
extern crate webicon;
#[macro_use]
extern crate lazy_static;
extern crate sxd_document;
#[macro_use]
extern crate rust_embed;
#[cfg(test)]
extern crate tempfile;

pub mod database;
mod default_portal;
mod error;
mod feed_api;
mod feed_api_implementations;
pub mod models;
mod schema;
mod util;

use database::DataBase;
use default_portal::DefaultPortal;
use error::{NewsFlashError, NewsFlashErrorKind};
use failure::ResultExt;
use feed_api::FeedApi;
use feed_api::{ApiMetadata, Portal};
use models::{
    Article, ArticleID, Category, CategoryID, Config, Feed, FeedID, FeedMapping, LoginData,
    LoginGUI, Marked, PluginID, Read, Tag, TagID, Tagging, Url, NEWSFLASH_TOPLEVEL,
};
use std::collections::hash_map::HashMap;
use std::path::PathBuf;
use std::rc::Rc;
use util::favicons::{FavIcon, FavIcons};
use util::opml;

type NewsFlashResult<T> = Result<T, NewsFlashError>;

pub struct NewsFlash {
    data_dir: PathBuf,
    db: Rc<DataBase>,
    api: Option<Box<FeedApi>>,
    config: Config,
    icons: FavIcons,
}

impl NewsFlash {
    pub fn list_backends() -> HashMap<PluginID, Box<ApiMetadata>> {
        feed_api_implementations::list()
    }

    pub fn new(data_dir: &PathBuf, id: Option<PluginID>) -> NewsFlashResult<Self> {
        // create data dir if it doesn't already exist
        std::fs::DirBuilder::new()
            .recursive(true)
            .create(&data_dir)
            .context(NewsFlashErrorKind::IO)?;

        let db = DataBase::new(data_dir).context(NewsFlashErrorKind::DataBase)?;
        let db = Rc::new(db);
        let api = match id {
            Some(id) => {
                let api = NewsFlash::load_backend(id, data_dir, db.clone())?;
                Some(api)
            }
            None => None,
        };
        let config = Config::open(&data_dir).context(NewsFlashErrorKind::IO)?;
        let base = NewsFlash {
            data_dir: data_dir.clone(),
            db: db,
            api: api,
            config: config,
            icons: FavIcons::new(&data_dir.join("icons")),
        };

        Ok(base)
    }

    fn load_backend(
        backend_id: PluginID,
        data_dir: &PathBuf,
        db: Rc<DataBase>,
    ) -> NewsFlashResult<Box<FeedApi>> {
        if let Some(meta_data) = NewsFlash::list_backends().get(&backend_id) {
            let portal = NewsFlash::default_portal(db).context(NewsFlashErrorKind::Portal)?;
            let config_path = data_dir.join(meta_data.config_name());
            let backend = meta_data
                .get_instance(config_path, portal)
                .context(NewsFlashErrorKind::LoadBackend)?;
            return Ok(backend);
        }
        Err(NewsFlashErrorKind::LoadBackend)?
    }

    fn default_portal(db: Rc<DataBase>) -> NewsFlashResult<Box<Portal>> {
        let portal = DefaultPortal::new(db);
        let portal = Box::new(portal);
        Ok(portal)
    }

    pub fn has_backend(&self) -> bool {
        self.api.is_some()
    }

    pub fn get_icon_info(&mut self, feed: &Feed) -> NewsFlashResult<FavIcon> {
        let info = self
            .icons
            .get_icon_info(feed)
            .context(NewsFlashErrorKind::Icon)?;
        Ok(info)
    }

    pub fn login_gui_description(&mut self, id: Option<PluginID>) -> NewsFlashResult<LoginGUI> {
        if let Some(id) = id {
            let api = NewsFlash::load_backend(id, &self.data_dir, self.db.clone())?;
            self.api = Some(api);
        }
        if let Some(api) = &self.api {
            let login_gui = api.login_gui().context(NewsFlashErrorKind::API)?;
            return Ok(login_gui);
        }
        Err(NewsFlashErrorKind::Unconfigured)?
    }

    pub fn login(&mut self, data: LoginData) -> NewsFlashResult<()> {
        if let Some(ref mut api) = self.api {
            api.login(data).context(NewsFlashErrorKind::API)?;
            return Ok(());
        }
        Err(NewsFlashErrorKind::Unconfigured)?
    }

    pub fn initial_sync(&mut self) -> NewsFlashResult<()> {
        if let Some(ref mut api) = self.api {
            let result = api.initial_sync().context(NewsFlashErrorKind::API)?;
            self.db
                .write_sync_result(result)
                .context(NewsFlashErrorKind::DataBase)?;
            return Ok(());
        }
        Err(NewsFlashErrorKind::Unconfigured)?
    }

    pub fn sync(&mut self) -> NewsFlashResult<()> {
        if let Some(ref mut api) = self.api {
            let now = chrono::Utc::now().naive_utc();
            let max_count = self
                .config
                .get_sync_amount()
                .context(NewsFlashErrorKind::Config)?;
            let last_sync = self
                .config
                .get_last_sync()
                .context(NewsFlashErrorKind::Config)?;

            let result = api
                .sync(max_count, last_sync)
                .context(NewsFlashErrorKind::API)?;
            self.db
                .write_sync_result(result)
                .context(NewsFlashErrorKind::DataBase)?;
            self.config
                .set_last_sync(now)
                .context(NewsFlashErrorKind::Config)?;

            return Ok(());
        }
        Err(NewsFlashErrorKind::Unconfigured)?
    }

    pub fn set_article_read(
        &mut self,
        articles: &Vec<ArticleID>,
        read: Read,
    ) -> NewsFlashResult<()> {
        if let Some(ref mut api) = self.api {
            api.set_article_read(&articles, read.clone())
                .context(NewsFlashErrorKind::API)?;
            self.db
                .set_article_read(&articles, read)
                .context(NewsFlashErrorKind::DataBase)?;
            return Ok(());
        }
        Err(NewsFlashErrorKind::Unconfigured)?
    }

    pub fn set_article_marked(
        &mut self,
        articles: &Vec<ArticleID>,
        marked: Marked,
    ) -> NewsFlashResult<()> {
        if let Some(ref mut api) = self.api {
            api.set_article_marked(&articles, marked.clone())
                .context(NewsFlashErrorKind::API)?;
            self.db
                .set_article_marked(&articles, marked)
                .context(NewsFlashErrorKind::DataBase)?;
            return Ok(());
        }
        Err(NewsFlashErrorKind::Unconfigured)?
    }

    pub fn set_feed_read(&mut self, feeds: &Vec<FeedID>) -> NewsFlashResult<()> {
        if let Some(ref mut api) = self.api {
            api.set_feed_read(&feeds).context(NewsFlashErrorKind::API)?;
            self.db
                .set_feed_read(&feeds)
                .context(NewsFlashErrorKind::DataBase)?;
            return Ok(());
        }
        Err(NewsFlashErrorKind::Unconfigured)?
    }

    pub fn set_category_read(&mut self, categories: &Vec<CategoryID>) -> NewsFlashResult<()> {
        if let Some(ref mut api) = self.api {
            api.set_category_read(&categories)
                .context(NewsFlashErrorKind::API)?;
            self.db
                .set_category_read(&categories)
                .context(NewsFlashErrorKind::DataBase)?;
            return Ok(());
        }
        Err(NewsFlashErrorKind::Unconfigured)?
    }

    pub fn set_tag_read(&mut self, tags: &Vec<TagID>) -> NewsFlashResult<()> {
        if let Some(ref mut api) = self.api {
            api.set_tag_read(tags).context(NewsFlashErrorKind::API)?;
            self.db
                .set_tag_read(tags)
                .context(NewsFlashErrorKind::DataBase)?;
            return Ok(());
        }
        Err(NewsFlashErrorKind::Unconfigured)?
    }

    pub fn set_all_read(&mut self) -> NewsFlashResult<()> {
        if let Some(ref mut api) = self.api {
            api.set_all_read().context(NewsFlashErrorKind::API)?;
            self.db
                .set_all_read()
                .context(NewsFlashErrorKind::DataBase)?;
            return Ok(());
        }
        Err(NewsFlashErrorKind::Unconfigured)?
    }

    pub fn add_feed(
        &mut self,
        url: Url,
        title: Option<&str>,
        category: Option<&CategoryID>,
    ) -> NewsFlashResult<Feed> {
        if let Some(ref mut api) = self.api {
            let feed = api
                .add_feed(&url, title, category)
                .context(NewsFlashErrorKind::API)?;
            let feed_id = feed.feed_id.clone();
            self.db
                .write_feeds(&vec![feed.clone()])
                .context(NewsFlashErrorKind::DataBase)?;

            if let Some(category_id) = category {
                let mapping = FeedMapping {
                    feed_id: feed_id,
                    category_id: category_id.clone(),
                };
                self.db
                    .write_mappings(&vec![mapping])
                    .context(NewsFlashErrorKind::DataBase)?;
            }
            return Ok(feed);
        }
        Err(NewsFlashErrorKind::Unconfigured)?
    }

    pub fn remove_feed(&mut self, feed: &Feed) -> NewsFlashResult<()> {
        if let Some(ref mut api) = self.api {
            // remove feed from backend
            api.remove_feed(&feed.feed_id)
                .context(NewsFlashErrorKind::API)?;

            // remove feed from db
            self.db
                .drop_feed(&feed.feed_id)
                .context(NewsFlashErrorKind::DataBase)?;

            // remove feed mappings from db
            let mappings = self
                .db
                .read_mappings(Some(&feed.feed_id), None)
                .context(NewsFlashErrorKind::DataBase)?;
            for mapping in mappings {
                self.db
                    .drop_mapping(&mapping)
                    .context(NewsFlashErrorKind::DataBase)?;
            }

            // remove articles from db
            let articles = self
                .db
                .read_articles(
                    None,
                    None,
                    None,
                    None,
                    None,
                    Some(&feed.feed_id),
                    None,
                    None,
                    None,
                    None,
                )
                .context(NewsFlashErrorKind::DataBase)?;
            let ids: Vec<ArticleID> = articles.into_iter().map(|a| a.article_id).collect();
            self.db
                .drop_articles(&ids)
                .context(NewsFlashErrorKind::DataBase)?;

            return Ok(());
        }
        Err(NewsFlashErrorKind::Unconfigured)?
    }

    pub fn move_feed(
        &mut self,
        feed: &Feed,
        from: &Category,
        to: &Category,
    ) -> NewsFlashResult<()> {
        if let Some(ref mut api) = self.api {
            api.move_feed(&feed.feed_id, &from.category_id, &to.category_id)
                .context(NewsFlashErrorKind::API)?;

            // drop mapping 'from'
            self.db
                .drop_mapping(&FeedMapping {
                    feed_id: feed.feed_id.clone(),
                    category_id: from.category_id.clone(),
                })
                .context(NewsFlashErrorKind::DataBase)?;

            // add mapping 'to'
            self.db
                .insert_mapping(&FeedMapping {
                    feed_id: feed.feed_id.clone(),
                    category_id: to.category_id.clone(),
                })
                .context(NewsFlashErrorKind::DataBase)?;
        }
        Err(NewsFlashErrorKind::Unconfigured)?
    }

    pub fn rename_feed(&mut self, feed: &Feed, new_title: &str) -> NewsFlashResult<Feed> {
        if let Some(ref mut api) = self.api {
            api.rename_feed(&feed.feed_id, new_title)
                .context(NewsFlashErrorKind::API)?;
            let mut feed = feed.clone();
            feed.label = new_title.to_owned();
            self.db
                .insert_feed(&feed)
                .context(NewsFlashErrorKind::DataBase)?;
        }
        Err(NewsFlashErrorKind::Unconfigured)?
    }

    pub fn add_category(
        &mut self,
        title: &str,
        parent: Option<&CategoryID>,
        sort_index: Option<i32>,
    ) -> NewsFlashResult<Category> {
        if let Some(ref mut api) = self.api {
            let category_id = api
                .add_category(title, parent)
                .context(NewsFlashErrorKind::DataBase)?;

            let category = Category {
                category_id: category_id,
                label: title.to_owned(),
                sort_index: sort_index,
                parent: match parent {
                    Some(parent) => parent.clone(),
                    None => NEWSFLASH_TOPLEVEL.clone(),
                },
            };

            self.db
                .insert_category(&category)
                .context(NewsFlashErrorKind::DataBase)?;
            return Ok(category);
        }
        Err(NewsFlashErrorKind::Unconfigured)?
    }

    pub fn remove_category(
        &mut self,
        category: &Category,
        remove_children: bool,
    ) -> NewsFlashResult<()> {
        if let Some(ref mut api) = self.api {
            api.remove_category(&category.category_id, remove_children)
                .context(NewsFlashErrorKind::API)?;
        } else {
            return Err(NewsFlashErrorKind::Unconfigured)?;
        }

        if remove_children {
            self.remove_category_from_db_recurse(category)?;
        } else {
            self.remove_category_from_db_move_children_up(category)?;
        }

        Ok(())
    }

    fn remove_category_from_db_move_children_up(&self, category: &Category) -> NewsFlashResult<()> {
        let parent_id = category.parent.clone();

        // map feeds of category as children of category parent
        let mappings = self
            .db
            .read_mappings(None, Some(&category.category_id))
            .context(NewsFlashErrorKind::DataBase)?;
        for mut mapping in mappings {
            self.db
                .drop_mapping(&mapping)
                .context(NewsFlashErrorKind::DataBase)?;
            mapping.category_id = parent_id.clone();
            self.db
                .insert_mapping(&mapping)
                .context(NewsFlashErrorKind::DataBase)?;
        }

        let child_categories: Vec<Category> = self
            .db
            .read_categories()
            .context(NewsFlashErrorKind::DataBase)?
            .into_iter()
            .filter(|category| category.parent == parent_id)
            .collect();

        let mutated_child_categories: Vec<Category> = child_categories
            .into_iter()
            .map(|mut category| {
                category.parent = parent_id.clone();
                category
            })
            .collect();

        self.db
            .insert_categories(&mutated_child_categories)
            .context(NewsFlashErrorKind::DataBase)?;
        Ok(())
    }

    fn remove_category_from_db_recurse(&self, category: &Category) -> NewsFlashResult<()> {
        // remove childen feeds
        let mappings = self
            .db
            .read_mappings(None, Some(&category.category_id))
            .context(NewsFlashErrorKind::DataBase)?;
        for mapping in mappings {
            self.db
                .drop_feed(&mapping.feed_id)
                .context(NewsFlashErrorKind::DataBase)?;
        }

        // remove category
        self.db
            .drop_category(category)
            .context(NewsFlashErrorKind::DataBase)?;

        // look for children categories and recurse
        let categories = self
            .db
            .read_categories()
            .context(NewsFlashErrorKind::DataBase)?;
        let child_categories: Vec<Category> = categories
            .into_iter()
            .filter(|category| category.parent == category.category_id)
            .collect();
        for child_category in child_categories {
            self.remove_category_from_db_recurse(&child_category)?;
        }

        Ok(())
    }

    pub fn rename_category(
        &mut self,
        category: &Category,
        new_title: &str,
    ) -> NewsFlashResult<Category> {
        if let Some(ref mut api) = self.api {
            api.rename_category(&category.category_id, new_title)
                .context(NewsFlashErrorKind::API)?;
            let mut category = category.clone();
            category.label = new_title.to_owned();
            self.db
                .insert_category(&category)
                .context(NewsFlashErrorKind::DataBase)?;
            return Ok(category);
        }
        Err(NewsFlashErrorKind::Unconfigured)?
    }

    pub fn move_category(&mut self, category: &Category, parent: &Category) -> NewsFlashResult<()> {
        if let Some(ref mut api) = self.api {
            api.move_category(&category.category_id, &parent.category_id)
                .context(NewsFlashErrorKind::API)?;
            let mut mutated_category = category.clone();
            mutated_category.parent = parent.category_id.clone();
            self.db
                .insert_category(&mutated_category)
                .context(NewsFlashErrorKind::DataBase)?;
            return Ok(());
        }
        Err(NewsFlashErrorKind::Unconfigured)?
    }

    pub fn add_tag(&mut self, title: &str, color: Option<i32>) -> NewsFlashResult<Tag> {
        if let Some(ref mut api) = self.api {
            let tag_id = api.add_tag(title).context(NewsFlashErrorKind::API)?;
            let tag = Tag {
                tag_id: tag_id,
                label: title.to_owned(),
                color: color,
            };
            self.db
                .insert_tag(&tag)
                .context(NewsFlashErrorKind::DataBase)?;
            return Ok(tag);
        }
        Err(NewsFlashErrorKind::Unconfigured)?
    }

    pub fn remove_tag(&mut self, tag: &Tag) -> NewsFlashResult<()> {
        if let Some(ref mut api) = self.api {
            api.remove_tag(&tag.tag_id)
                .context(NewsFlashErrorKind::API)?;
            self.db
                .drop_tag(&tag)
                .context(NewsFlashErrorKind::DataBase)?;
            return Ok(());
        }
        Err(NewsFlashErrorKind::Unconfigured)?
    }

    pub fn rename_tag(&mut self, tag: &Tag, new_title: &str) -> NewsFlashResult<Tag> {
        if let Some(ref mut api) = self.api {
            let new_id = api
                .rename_tag(&tag.tag_id, new_title)
                .context(NewsFlashErrorKind::API)?;

            self.db
                .drop_tag(&tag)
                .context(NewsFlashErrorKind::DataBase)?;
            let mutated_tag = Tag {
                tag_id: new_id.clone(),
                label: new_title.to_owned(),
                color: tag.color,
            };
            self.db
                .insert_tag(&mutated_tag)
                .context(NewsFlashErrorKind::DataBase)?;

            let taggings = self
                .db
                .read_taggings(None, Some(&tag.tag_id))
                .context(NewsFlashErrorKind::DataBase)?;
            let mutated_taggings: Vec<Tagging> = taggings
                .into_iter()
                .map(|mut tagging| {
                    tagging.tag_id = new_id.clone();
                    tagging
                })
                .collect();
            self.db
                .drop_taggings_of_tag(tag)
                .context(NewsFlashErrorKind::DataBase)?;
            self.db
                .insert_taggings(&mutated_taggings)
                .context(NewsFlashErrorKind::DataBase)?;
            return Ok(mutated_tag);
        }
        Err(NewsFlashErrorKind::Unconfigured)?
    }

    pub fn tag_article(&mut self, article: &Article, tag: &Tag) -> NewsFlashResult<()> {
        if let Some(ref mut api) = self.api {
            api.tag_article(&article.article_id, &tag.tag_id)
                .context(NewsFlashErrorKind::API)?;
            let tagging = Tagging {
                article_id: article.article_id.clone(),
                tag_id: tag.tag_id.clone(),
            };
            self.db
                .insert_tagging(&tagging)
                .context(NewsFlashErrorKind::DataBase)?;
            return Ok(());
        }
        Err(NewsFlashErrorKind::Unconfigured)?
    }

    pub fn untag_article(&mut self, article: &Article, tag: &Tag) -> NewsFlashResult<()> {
        if let Some(ref mut api) = self.api {
            api.untag_article(&article.article_id, &tag.tag_id)
                .context(NewsFlashErrorKind::API)?;
            let tagging = Tagging {
                article_id: article.article_id.clone(),
                tag_id: tag.tag_id.clone(),
            };
            self.db
                .drop_tagging(&tagging)
                .context(NewsFlashErrorKind::DataBase)?;
            return Ok(());
        }
        Err(NewsFlashErrorKind::Unconfigured)?
    }

    pub fn import_opml(&mut self, opml: &str, parse_feeds: bool) -> NewsFlashResult<()> {
        if let Some(ref mut api) = self.api {
            api.import_opml(opml)
                .context(NewsFlashErrorKind::API)
                .context(NewsFlashErrorKind::API)?;
            let (categories, feeds, mappings) =
                opml::parse_opml(opml, parse_feeds).context(NewsFlashErrorKind::OPML)?;
            self.db
                .insert_categories(&categories)
                .context(NewsFlashErrorKind::DataBase)?;
            self.db
                .insert_feeds(&feeds)
                .context(NewsFlashErrorKind::DataBase)?;
            self.db
                .insert_mappings(&mappings)
                .context(NewsFlashErrorKind::DataBase)?;
            return Ok(());
        }
        Err(NewsFlashErrorKind::Unconfigured)?
    }

    pub fn export_opml(&self) -> NewsFlashResult<String> {
        let categories = self
            .db
            .read_categories()
            .context(NewsFlashErrorKind::DataBase)?;
        let feeds = self.db.read_feeds().context(NewsFlashErrorKind::DataBase)?;
        let mappings = self
            .db
            .read_mappings(None, None)
            .context(NewsFlashErrorKind::DataBase)?;

        let opml_string =
            opml::generate_opml(&categories, &feeds, &mappings).context(NewsFlashErrorKind::OPML)?;
        Ok(opml_string)
    }
}
