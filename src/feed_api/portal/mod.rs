mod error;

pub use self::error::{PortalError, PortalErrorKind};
use models::{Article, ArticleID, Category, Feed, FeedMapping, Headline, Tag};

pub type PortalResult<T> = Result<T, PortalError>;

pub trait Portal {
    fn get_headlines(&self, ids: Vec<ArticleID>) -> PortalResult<Vec<Headline>>;
    fn get_articles(&self, ids: Vec<ArticleID>) -> PortalResult<Vec<Article>>;
    fn get_feeds(&self) -> PortalResult<Vec<Feed>>;
    fn get_categories(&self) -> PortalResult<Vec<Category>>;
    fn get_mappings(&self) -> PortalResult<Vec<FeedMapping>>;
    fn get_tags(&self) -> PortalResult<Vec<Tag>>;
}
