pub mod error;
pub mod portal;

use chrono::NaiveDateTime;
use models::{
    ArticleID, CategoryID, Feed, FeedID, LoginData, LoginGUI, Marked, PluginCapabilities, PluginID,
    PluginMetadata, Read, SyncResult, TagID, Url,
};
use std::path::PathBuf;

pub use self::error::{FeedApiError, FeedApiErrorKind};
pub use self::portal::Portal;

pub type FeedApiResult<T> = Result<T, FeedApiError>;

pub trait ApiMetadata {
    fn id(&self) -> PluginID;
    fn metadata(&self) -> PluginMetadata;
    fn config_name(&self) -> &str;
    fn get_instance(&self, config: PathBuf, portal: Box<Portal>) -> FeedApiResult<Box<FeedApi>>;
}

pub trait FeedApi {
    fn features(&self) -> FeedApiResult<PluginCapabilities>;
    fn login_gui(&self) -> FeedApiResult<LoginGUI>;
    fn has_user_configured(&self) -> FeedApiResult<bool>;
    fn is_logged_in(&mut self) -> FeedApiResult<bool>;
    fn login(&mut self, data: LoginData) -> FeedApiResult<()>;
    fn initial_sync(&mut self) -> FeedApiResult<SyncResult>;
    fn sync(&mut self, max_count: u32, last_sync: NaiveDateTime) -> FeedApiResult<SyncResult>;
    fn unread_count(&mut self) -> FeedApiResult<u32>;
    fn set_article_read(&mut self, articles: &Vec<ArticleID>, read: Read) -> FeedApiResult<()>;
    fn set_article_marked(
        &mut self,
        articles: &Vec<ArticleID>,
        marked: Marked,
    ) -> FeedApiResult<()>;
    fn set_feed_read(&mut self, feeds: &Vec<FeedID>) -> FeedApiResult<()>;
    fn set_category_read(&mut self, categories: &Vec<CategoryID>) -> FeedApiResult<()>;
    fn set_tag_read(&mut self, tags: &Vec<TagID>) -> FeedApiResult<()>;
    fn set_all_read(&mut self) -> FeedApiResult<()>;
    fn add_feed(
        &mut self,
        url: &Url,
        title: Option<&str>,
        category: Option<&CategoryID>,
    ) -> FeedApiResult<Feed>;
    fn remove_feed(&mut self, id: &FeedID) -> FeedApiResult<()>;
    fn move_feed(
        &mut self,
        feed_id: &FeedID,
        from: &CategoryID,
        to: &CategoryID,
    ) -> FeedApiResult<()>;
    fn rename_feed(&mut self, feed_id: &FeedID, new_title: &str) -> FeedApiResult<FeedID>;
    fn add_category(
        &mut self,
        title: &str,
        parent: Option<&CategoryID>,
    ) -> FeedApiResult<CategoryID>;
    fn remove_category(&mut self, id: &CategoryID, remove_children: bool) -> FeedApiResult<()>;
    fn rename_category(&mut self, id: &CategoryID, new_title: &str) -> FeedApiResult<CategoryID>;
    fn move_category(&mut self, id: &CategoryID, parent: &CategoryID) -> FeedApiResult<()>;
    fn import_opml(&mut self, opml: &str) -> FeedApiResult<()>;
    fn add_tag(&mut self, title: &str) -> FeedApiResult<TagID>;
    fn remove_tag(&mut self, id: &TagID) -> FeedApiResult<()>;
    fn rename_tag(&mut self, id: &TagID, new_title: &str) -> FeedApiResult<TagID>;
    fn tag_article(&mut self, article_id: &ArticleID, tag_id: &TagID) -> FeedApiResult<()>;
    fn untag_article(&mut self, article_id: &ArticleID, tag_id: &TagID) -> FeedApiResult<()>;
}
