use models::{PluginID, Url};

pub struct PluginMetadata {
    pub id: PluginID,
    pub name: String,
    pub icon: Option<String>,
    pub icon_symbolic: Option<String>,
    pub website: Option<Url>,
    pub service_type: ServiceType,
    pub license_type: ServiceLicense,
    pub service_price: ServicePrice,
}

pub enum ServiceType {
    Local,
    Remote { self_hosted: bool },
}

pub enum ServiceLicense {
    FreeSoftware,
    Proprietary,
}

pub enum ServicePrice {
    Free,
    Paid,
    PaidPremimum,
}
