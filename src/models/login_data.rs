use url::Url;

pub enum LoginData {
    Password(PasswordLogin),
    OAuth(OAuthData),
}

pub struct OAuthData {
    pub url: Url,
}

pub struct PasswordLogin {
    pub url: Option<Url>,
    pub user: String,
    pub password: String,
    pub http_user: Option<String>,
    pub http_password: Option<String>,
}
