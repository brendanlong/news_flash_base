use models::{CategoryID, FeedID, Url};
use schema::{feed_mapping, feeds};

#[derive(Identifiable, Clone, Insertable, Queryable, PartialEq, Debug)]
#[primary_key(feed_id)]
#[table_name = "feeds"]
pub struct Feed {
    pub feed_id: FeedID,
    pub label: String,
    pub website: Option<Url>,
    pub feed_url: Option<Url>,
    pub icon_url: Option<Url>,
    #[column_name = "order_id"]
    pub sort_index: Option<i32>,
}

#[derive(Identifiable, Insertable, Associations, Queryable, PartialEq, Debug)]
#[primary_key(feed_id)]
#[table_name = "feed_mapping"]
#[belongs_to(Feed, foreign_key = "feed_id")]
pub struct FeedMapping {
    pub feed_id: FeedID,
    pub category_id: CategoryID,
}
