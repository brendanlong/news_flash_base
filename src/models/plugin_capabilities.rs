bitflags! {
    #[derive(Default)]
    pub struct PluginCapabilities: u32 {
        const ADD_REMOVE_FEEDS      = 0b00000001;
        const SUPPORT_CATEGORIES    = 0b00000010;
        const MODIFY_CATEGORIES     = 0b00000100;
        const SUPPORT_TAGS          = 0b00001000;
    }
}
