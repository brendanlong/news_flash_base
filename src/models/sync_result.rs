use models::{Article, Category, Enclosure, Feed, FeedMapping, Headline, Tag, Tagging};

pub struct SyncResult {
    pub feeds: Option<Vec<Feed>>,
    pub categories: Option<Vec<Category>>,
    pub mappings: Option<Vec<FeedMapping>>,
    pub tags: Option<Vec<Tag>>,
    pub headlines: Option<Vec<Headline>>,
    pub articles: Option<Vec<Article>>,
    pub enclosures: Option<Vec<Enclosure>>,
    pub taggings: Option<Vec<Tagging>>,
}
