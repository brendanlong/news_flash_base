use chrono::NaiveDateTime;
use failure::Error;
use serde_json;
use std::fs;
use std::path::PathBuf;

#[derive(Debug, Serialize, Deserialize)]
pub struct Config {
    sync_amount: u32,
    #[serde(with = "json_time")]
    last_sync: NaiveDateTime,
    #[serde(skip_serializing)]
    #[serde(skip_deserializing)]
    path: PathBuf,
}

impl Config {
    pub fn open(path: &PathBuf) -> Result<Self, Error> {
        let path = path.join("base_config.json");
        let data = fs::read_to_string(&path)?;
        let mut config: Self = serde_json::from_str(&data)?;
        config.path = path.clone();
        Ok(config)
    }

    fn write(&self) -> Result<(), Error> {
        let data = serde_json::to_value(self)?;
        let data = data.as_str();

        if let Some(data) = data {
            let path = self.path.join("base_config.json");
            fs::write(path, data)?;
            return Ok(());
        }

        Err(format_err!("some error"))
    }

    pub fn get_sync_amount(&self) -> Result<u32, Error> {
        Ok(self.sync_amount)
    }

    pub fn set_sync_amount(&mut self, amount: u32) -> Result<(), Error> {
        self.sync_amount = amount;
        self.write()?;
        Ok(())
    }

    pub fn get_last_sync(&self) -> Result<NaiveDateTime, Error> {
        Ok(self.last_sync)
    }

    pub fn set_last_sync(&mut self, time: NaiveDateTime) -> Result<(), Error> {
        self.last_sync = time;
        self.write()?;
        Ok(())
    }
}

mod json_time {
    use chrono::{DateTime, NaiveDateTime, Utc};
    use serde::{de::Error, Deserialize, Deserializer, Serialize, Serializer};

    pub fn time_to_json(t: NaiveDateTime) -> String {
        DateTime::<Utc>::from_utc(t, Utc).to_rfc3339()
    }

    pub fn serialize<S: Serializer>(
        time: &NaiveDateTime,
        serializer: S,
    ) -> Result<S::Ok, S::Error> {
        time_to_json(time.clone()).serialize(serializer)
    }

    pub fn deserialize<'de, D: Deserializer<'de>>(
        deserializer: D,
    ) -> Result<NaiveDateTime, D::Error> {
        let time: String = Deserialize::deserialize(deserializer)?;
        Ok(DateTime::parse_from_rfc3339(&time)
            .map_err(D::Error::custom)?
            .naive_utc())
    }
}
