use url::Url;

pub enum LoginGUI {
    Password(PasswordLoginGUI),
    OAuth(OAuthLoginGUI),
}

pub struct OAuthLoginGUI {
    pub login_website: Option<Url>,
    pub catch_redirect: Option<String>,
}

pub struct PasswordLoginGUI {
    pub url: Option<Url>,
    pub user: Option<String>,
    pub password: Option<String>,
    pub http_auth: bool,
}
