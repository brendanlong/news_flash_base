use models::CategoryID;
use schema::categories;

#[derive(Identifiable, Clone, Insertable, Queryable, PartialEq, Debug)]
#[primary_key(category_id)]
#[table_name = "categories"]
pub struct Category {
    pub category_id: CategoryID,
    pub label: String,
    #[column_name = "order_id"]
    pub sort_index: Option<i32>,
    #[column_name = "parent_id"]
    pub parent: CategoryID,
}
