use models::{ArticleID, TagID};
use schema::{taggings, tags};

#[derive(Identifiable, Insertable, Queryable, PartialEq, Debug)]
#[primary_key(tag_id)]
#[table_name = "tags"]
pub struct Tag {
    pub tag_id: TagID,
    pub label: String,
    pub color: Option<i32>,
}

#[derive(Identifiable, Insertable, Queryable, PartialEq, Debug)]
#[primary_key(article_id)]
#[table_name = "taggings"]
pub struct Tagging {
    pub article_id: ArticleID,
    pub tag_id: TagID,
}
