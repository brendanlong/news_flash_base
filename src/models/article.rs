use chrono::NaiveDateTime;
use diesel::backend::Backend;
use diesel::deserialize;
use diesel::deserialize::FromSql;
use diesel::serialize;
use diesel::serialize::{Output, ToSql};
use diesel::sql_types::Integer;
use diesel::sqlite::Sqlite;
use failure::ResultExt;
use models::error::{ModelError, ModelErrorKind};
use models::{ArticleID, FeedID, Url};
use schema::articles;
use std;
use std::io::Write;
use std::path::PathBuf;

#[derive(Identifiable, Insertable, Queryable, PartialEq, Debug)]
#[primary_key(article_id)]
#[table_name = "articles"]
pub struct Article {
    pub article_id: ArticleID,
    pub title: Option<String>,
    pub author: Option<String>,
    pub feed_id: FeedID,
    pub url: Option<Url>,
    #[column_name = "timestamp"]
    pub date: NaiveDateTime,
    pub html: Option<String>,
    pub summary: Option<String>,
    pub direction: Option<Direction>,
    pub unread: Read,
    pub marked: Marked,
}

impl Article {
    pub fn save_html(&self, path: &PathBuf) -> Result<(), ModelError> {
        if let Some(ref html) = self.html {
            if let Ok(()) = std::fs::create_dir_all(&path) {
                let mut file_name = match self.title.clone() {
                    Some(file_name) => file_name,
                    None => "Unknown Title".to_owned(),
                };
                file_name.push_str(".html");
                let path = path.join(file_name);
                let mut html_file = std::fs::File::create(&path).context(ModelErrorKind::IO)?;
                html_file
                    .write_all(html.as_bytes())
                    .context(ModelErrorKind::IO)?;
            }
        }

        Err(ModelErrorKind::IO)?
    }
}

//------------------------------------------------------------------

#[derive(PartialEq, Clone, Debug, AsExpression, FromSqlRow)]
#[sql_type = "Integer"]
pub enum Direction {
    LeftToRight,
    RightToLeft,
}

impl Direction {
    pub fn to_int(&self) -> i32 {
        self.clone() as i32
    }

    pub fn from_int(int: i32) -> Self {
        match int {
            0 => Direction::LeftToRight,
            1 => Direction::RightToLeft,

            _ => Direction::LeftToRight,
        }
    }
}

impl FromSql<Integer, Sqlite> for Direction {
    fn from_sql(bytes: Option<&<Sqlite as Backend>::RawValue>) -> deserialize::Result<Self> {
        let int = not_none!(bytes).read_integer();
        Ok(Direction::from_int(int))
    }
}

impl ToSql<Integer, Sqlite> for Direction {
    fn to_sql<W: Write>(&self, out: &mut Output<W, Sqlite>) -> serialize::Result {
        ToSql::<Integer, Sqlite>::to_sql(&self.to_int(), out)
    }
}

//------------------------------------------------------------------

#[derive(PartialEq, Clone, Debug, AsExpression, FromSqlRow)]
#[sql_type = "Integer"]
pub enum Read {
    Read,
    Unread,
}

impl Read {
    pub fn to_int(&self) -> i32 {
        self.clone() as i32
    }

    pub fn from_int(int: i32) -> Self {
        match int {
            0 => Read::Read,
            1 => Read::Unread,

            _ => Read::Read,
        }
    }
}

impl FromSql<Integer, Sqlite> for Read {
    fn from_sql(bytes: Option<&<Sqlite as Backend>::RawValue>) -> deserialize::Result<Self> {
        let int = not_none!(bytes).read_integer();
        Ok(Read::from_int(int))
    }
}

impl ToSql<Integer, Sqlite> for Read {
    fn to_sql<W: Write>(&self, out: &mut Output<W, Sqlite>) -> serialize::Result {
        ToSql::<Integer, Sqlite>::to_sql(&self.to_int(), out)
    }
}

//------------------------------------------------------------------

#[derive(PartialEq, Clone, Debug, AsExpression, FromSqlRow)]
#[sql_type = "Integer"]
pub enum Marked {
    Marked,
    Unmarked,
}

impl Marked {
    pub fn to_int(&self) -> i32 {
        self.clone() as i32
    }

    pub fn from_int(int: i32) -> Self {
        match int {
            0 => Marked::Marked,
            1 => Marked::Unmarked,

            _ => Marked::Unmarked,
        }
    }
}

impl FromSql<Integer, Sqlite> for Marked {
    fn from_sql(bytes: Option<&<Sqlite as Backend>::RawValue>) -> deserialize::Result<Self> {
        let int = not_none!(bytes).read_integer();
        Ok(Marked::from_int(int))
    }
}

impl ToSql<Integer, Sqlite> for Marked {
    fn to_sql<W: Write>(&self, out: &mut Output<W, Sqlite>) -> serialize::Result {
        ToSql::<Integer, Sqlite>::to_sql(&self.to_int(), out)
    }
}
