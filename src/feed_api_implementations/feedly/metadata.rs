use super::config::FeedlySecrets;
use super::{Feedly, FeedlyApi};
use failure::ResultExt;
use feed_api::{ApiMetadata, FeedApi, FeedApiErrorKind, FeedApiResult, Portal};
use models::{PluginID, PluginMetadata, ServiceLicense, ServicePrice, ServiceType, Url};
use serde_json;
use std::path::PathBuf;
use std::str;

#[derive(RustEmbed)]
#[folder = "src/feed_api_implementations/feedly"]
struct Secret;

pub struct FeedlyMetadata;

impl ApiMetadata for FeedlyMetadata {
    fn id(&self) -> PluginID {
        PluginID::new("feedly")
    }

    fn metadata(&self) -> PluginMetadata {
        PluginMetadata {
            id: self.id(),
            name: String::from("feedly"),
            icon: Some(String::from("feed-service-feedly")),
            icon_symbolic: Some(String::from("feed-service-feedly-symbolic")),
            website: match Url::parse("http://feedly.com/") {
                Ok(website) => Some(website),
                Err(_) => None,
            },
            service_type: ServiceType::Remote { self_hosted: false },
            license_type: ServiceLicense::Proprietary,
            service_price: ServicePrice::PaidPremimum,
        }
    }

    fn config_name(&self) -> &str {
        "feedly.json"
    }

    fn get_instance(&self, config: PathBuf, portal: Box<Portal>) -> FeedApiResult<Box<FeedApi>> {
        let config = Feedly::load_config(config).context(FeedApiErrorKind::Config)?;
        let token_expires = FeedlyApi::parse_expiration_date(config.token_expires)
            .context(FeedApiErrorKind::Config)?;
        let secret_data = Secret::get("secrets.json").ok_or(FeedApiErrorKind::Secret)?;
        let secret_string = str::from_utf8(&secret_data).context(FeedApiErrorKind::Secret)?;
        let secret_struct: FeedlySecrets =
            serde_json::from_str(secret_string).context(FeedApiErrorKind::Secret)?;

        let api = FeedlyApi::new(
            secret_struct.client_id.clone(),
            secret_struct.client_secret.clone(),
            config.access_token.clone(),
            config.refresh_token.clone(),
            token_expires,
        ).context(FeedApiErrorKind::Api)?;

        let feedly = Feedly {
            api: api,
            portal: portal,
            logged_in: None,
        };
        let feedly = Box::new(feedly);

        Ok(feedly)
    }
}
