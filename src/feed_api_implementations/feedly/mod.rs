pub mod config;
pub mod metadata;

use self::config::AccountConfig;
use chrono::NaiveDateTime;
use failure::ResultExt;
use feed_api::{FeedApi, FeedApiErrorKind, FeedApiResult, Portal};
use feedly_api::models::{
    Category as FeedlyCategory, Content as FeedlyContent, Entry, Link, Subscription,
    SubscriptionInput, Tag as FeedlyTag,
};
use feedly_api::FeedlyApi;
use models;
use models::{
    Article, ArticleID, Category, CategoryID, Direction, Enclosure, EnclosureType, Feed, FeedID,
    FeedMapping, LoginData, LoginGUI, OAuthLoginGUI, PluginCapabilities, SyncResult, Tag, TagID,
    Tagging, Url, NEWSFLASH_TOPLEVEL,
};
use regex::Regex;
use serde_json;
use std::fs::File;
use std::io::Read;
use std::path::PathBuf;
use util;

pub struct Feedly {
    api: FeedlyApi,
    portal: Box<Portal>,
    logged_in: Option<bool>,
}

impl Feedly {
    fn load_config(config: PathBuf) -> FeedApiResult<AccountConfig> {
        let mut contents = String::new();
        let mut config = File::open(config).context(FeedApiErrorKind::IO)?;
        config
            .read_to_string(&mut contents)
            .context(FeedApiErrorKind::IO)?;
        let config: AccountConfig =
            serde_json::from_str(&contents).context(FeedApiErrorKind::Config)?;
        Ok(config)
    }

    fn convert_category_vec(categories: Vec<FeedlyCategory>) -> Vec<Category> {
        categories
            .iter()
            .enumerate()
            .map(|(i, c)| Category {
                category_id: CategoryID::new(&c.id),
                label: {
                    let mut label = "Unknown".to_string();
                    if let Some(ref l) = c.label {
                        label = l.clone();
                    } else {
                        if let Ok(regex) = Regex::new(r#"(?<=category\/).*$"#) {
                            if let Some(captures) = regex.captures(&c.id) {
                                if let Some(regex_match) = captures.get(1) {
                                    label = regex_match.as_str().to_owned();
                                }
                            }
                        }
                    }
                    label
                },
                sort_index: Some(i as i32),
                parent: NEWSFLASH_TOPLEVEL.clone(),
            })
            .collect()
    }

    fn convert_tag_vec(tags: Vec<FeedlyTag>) -> Vec<Tag> {
        tags.iter()
            .map(|t| Tag {
                tag_id: TagID::new(&t.id),
                color: None,
                label: match t.label {
                    Some(ref label) => label.clone(),
                    None => {
                        let mut label = "Unknown".to_string();
                        if let Some(ref l) = t.label {
                            label = l.clone();
                        } else {
                            if let Ok(regex) = Regex::new(r#"(?<=tag\/).*$"#) {
                                if let Some(captures) = regex.captures(&t.id) {
                                    if let Some(regex_match) = captures.get(1) {
                                        label = regex_match.as_str().to_owned();
                                    }
                                }
                            }
                        }
                        label
                    }
                },
            })
            .collect()
    }

    fn convert_subscription_vec(subscriptions: Vec<Subscription>) -> (Vec<Feed>, Vec<FeedMapping>) {
        let mut mappings: Vec<FeedMapping> = Vec::new();
        let feeds = subscriptions
            .iter()
            .enumerate()
            .map(|(i, f)| {
                if let Some(ref categories) = f.categories {
                    let mut feed_mappings = categories
                        .iter()
                        .map(|c| FeedMapping {
                            feed_id: FeedID::new(&f.id),
                            category_id: CategoryID::new(&c.id),
                        })
                        .collect::<Vec<FeedMapping>>();
                    mappings.append(&mut feed_mappings);
                }

                Feed {
                    feed_id: FeedID::new(&f.id),
                    label: f.title.clone(),
                    website: match f.website {
                        Some(ref url) => match Url::parse(&url) {
                            Ok(url) => Some(url),
                            Err(_) => None,
                        },
                        None => None,
                    },
                    feed_url: None,
                    icon_url: match f.icon_url {
                        Some(ref url) => match Url::parse(&url) {
                            Ok(url) => Some(url),
                            Err(_) => None,
                        },
                        None => match f.visual_url {
                            Some(ref url) => match Url::parse(&url) {
                                Ok(url) => Some(url),
                                Err(_) => None,
                            },
                            None => None,
                        },
                    },
                    sort_index: Some(i as i32),
                }
            })
            .collect();

        (feeds, mappings)
    }

    fn convert_entry_vec(
        entries: Vec<Entry>,
        read_tag: &str,
        marked_tag: &str,
    ) -> (Vec<Article>, Vec<Enclosure>, Vec<Tagging>) {
        let mut enclosures: Vec<Enclosure> = Vec::new();
        let mut taggings: Vec<Tagging> = Vec::new();
        let articles = entries
            .iter()
            .map(|e| {
                if let Some(ref mut article_enclosures) =
                    Feedly::convert_enclosures(&e.enclosure, ArticleID::new(&e.id))
                {
                    enclosures.append(article_enclosures);
                }

                if let Some(ref tag_vec) = e.tags {
                    let mut article_taggings: Vec<Tagging> = tag_vec
                        .iter()
                        .filter(|t| !t.id.contains("global."))
                        .map(|t| Tagging {
                            article_id: ArticleID::new(&e.id),
                            tag_id: TagID::new(&t.id),
                        })
                        .collect();
                    taggings.append(&mut article_taggings);
                }

                let (html, direction) = match Feedly::convert_content(&e.content) {
                    Some((html, direction)) => (Some(html), Some(direction)),
                    None => (None, None),
                };

                Article {
                    article_id: ArticleID::new(&e.id),
                    title: e.title.clone(),
                    author: e.author.clone(),
                    feed_id: match e.origin {
                        Some(ref origin) => FeedID::new(&origin.stream_id),
                        None => FeedID::new("None"),
                    },
                    url: match e.alternate {
                        Some(ref alternates) => match alternates.first() {
                            Some(link_obj) => match Url::parse(&link_obj.href) {
                                Ok(url) => Some(url),
                                Err(_) => None,
                            },
                            None => None,
                        },
                        None => None,
                    },
                    date: NaiveDateTime::from_timestamp(e.crawled as i64, 0),
                    html: html,
                    summary: match Feedly::convert_content(&e.summary) {
                        Some((summary, _)) => Some(summary),
                        None => None,
                    },
                    direction: direction,
                    unread: match e.tags {
                        Some(ref tags) => match tags.iter().find(|ref t| t.id.contains(read_tag)) {
                            Some(_) => models::Read::Read,
                            None => models::Read::Unread,
                        },
                        None => models::Read::Unread,
                    },
                    marked: match e.tags {
                        Some(ref tags) => match tags.iter().find(|ref t| t.id.contains(marked_tag))
                        {
                            Some(_) => models::Marked::Marked,
                            None => models::Marked::Unmarked,
                        },
                        None => models::Marked::Unmarked,
                    },
                }
            })
            .collect();

        (articles, enclosures, taggings)
    }

    fn convert_content(content: &Option<FeedlyContent>) -> Option<(String, Direction)> {
        match content {
            &Some(ref c) => {
                let direction = match c.direction {
                    Some(ref direction) => {
                        let mut dir = Direction::LeftToRight;
                        if direction == "rtl" {
                            dir = Direction::RightToLeft;
                        }
                        dir
                    }
                    None => Direction::LeftToRight,
                };

                Some((c.content.clone(), direction))
            }
            &None => None,
        }
    }

    fn convert_enclosures(
        enclosures: &Option<Vec<Link>>,
        article_id: ArticleID,
    ) -> Option<Vec<Enclosure>> {
        match enclosures {
            &Some(ref enclosure_vec) => {
                let res = enclosure_vec
                    .iter()
                    .map(|enc| Feedly::convert_enclosure(enc, &article_id))
                    .collect::<Result<Vec<Enclosure>, _>>();
                match res {
                    Ok(res) => Some(res),
                    Err(_) => None,
                }
            }
            &None => None,
        }
    }

    fn convert_enclosure(enc: &Link, article_id: &ArticleID) -> FeedApiResult<Enclosure> {
        let url = Url::parse(&enc.href).context(FeedApiErrorKind::Url)?;
        Ok(Enclosure {
            article_id: article_id.clone(),
            url: url,
            enclosure_type: match enc._type {
                Some(ref type_string) => EnclosureType::from_string(&type_string),
                None => EnclosureType::File,
            },
        })
    }

    fn get_articles(
        &mut self,
        stream_id: &str,
        count: Option<u32>,
        ranked: Option<&str>,
        unread_only: Option<bool>,
        newer_than: Option<u64>,
    ) -> FeedApiResult<(Vec<Article>, Vec<Enclosure>, Vec<Tagging>)> {
        let mut continuation: Option<String> = None;
        let mut articles: Vec<Article> = Vec::new();
        let mut enclosures: Vec<Enclosure> = Vec::new();
        let mut taggings: Vec<Tagging> = Vec::new();
        let tag_read = self.api.tag_read().context(FeedApiErrorKind::Api)?;
        let tag_marked = self.api.tag_marked().context(FeedApiErrorKind::Api)?;

        while {
            let stream = self
                .api
                .get_stream(
                    stream_id,
                    continuation,
                    count,
                    ranked,
                    unread_only,
                    newer_than,
                )
                .context(FeedApiErrorKind::Api)?;
            let (mut converted_articles, mut converted_enclosures, mut converted_taggings) =
                Feedly::convert_entry_vec(stream.items, &tag_read, &tag_marked);
            articles.append(&mut converted_articles);
            enclosures.append(&mut converted_enclosures);
            taggings.append(&mut converted_taggings);
            continuation = stream.continuation;

            continuation.is_some()
        } {}

        Ok((articles, enclosures, taggings))
    }

    fn vec_to_option<T>(vector: Vec<T>) -> Option<Vec<T>> {
        match vector.is_empty() {
            true => None,
            false => Some(vector),
        }
    }
}

impl FeedApi for Feedly {
    fn features(&self) -> FeedApiResult<PluginCapabilities> {
        Ok(PluginCapabilities::ADD_REMOVE_FEEDS
            | PluginCapabilities::SUPPORT_CATEGORIES
            | PluginCapabilities::MODIFY_CATEGORIES
            | PluginCapabilities::SUPPORT_TAGS)
    }

    fn has_user_configured(&self) -> FeedApiResult<bool> {
        Ok(self.api.has_access_token())
    }

    fn is_logged_in(&mut self) -> FeedApiResult<bool> {
        if let Some(logged_in) = self.logged_in {
            return Ok(logged_in);
        }

        Err(FeedApiErrorKind::Unknown)?
    }

    fn login_gui(&self) -> FeedApiResult<LoginGUI> {
        let login_url = self.api.login_url().context(FeedApiErrorKind::Api)?;

        Ok(LoginGUI::OAuth(OAuthLoginGUI {
            login_website: Some(login_url),
            catch_redirect: Some(String::from(self.api.redirect_url().as_str())),
        }))
    }

    fn login(&mut self, data: LoginData) -> FeedApiResult<()> {
        if let LoginData::OAuth(data) = data {
            match FeedlyApi::parse_redirected_url(data.url) {
                Ok(auth_code) => match self.api.request_auth_token(auth_code) {
                    Ok(_) => {
                        self.logged_in = Some(true);
                        return Ok(());
                    }
                    Err(e) => {
                        self.logged_in = Some(false);
                        return Err(e).context(FeedApiErrorKind::Login)?;
                    }
                },
                Err(e) => {
                    self.logged_in = None;
                    return Err(e).context(FeedApiErrorKind::Login)?;
                }
            }
        }

        self.logged_in = None;
        Err(FeedApiErrorKind::Login)?
    }

    fn initial_sync(&mut self) -> FeedApiResult<SyncResult> {
        let tag_marked = self.api.tag_marked().context(FeedApiErrorKind::Api)?;
        let tag_all = self.api.category_all().context(FeedApiErrorKind::Api)?;

        let categories = self.api.get_categories().context(FeedApiErrorKind::Api)?;
        let categories = Feedly::convert_category_vec(categories);

        let feeds = self.api.get_subsriptions().context(FeedApiErrorKind::Api)?;
        let (feeds, mappings) = Feedly::convert_subscription_vec(feeds);

        let tags = self.api.get_tags().context(FeedApiErrorKind::Api)?;
        let tags = Feedly::convert_tag_vec(tags);

        let mut articles: Vec<Article> = Vec::new();
        let mut enclosures: Vec<Enclosure> = Vec::new();
        let mut taggings: Vec<Tagging> = Vec::new();

        // get starred articles
        let (mut starred_articles, mut starred_enclosures, mut starred_taggings) =
            self.get_articles(&tag_marked, Some(200), None, None, None)?;
        articles.append(&mut starred_articles);
        enclosures.append(&mut starred_enclosures);
        taggings.append(&mut starred_taggings);

        // get tagged articles
        for tag in &tags {
            let (mut tag_articles, mut tag_enclosures, mut tag_taggings) =
                self.get_articles(tag.tag_id.to_str(), Some(200), None, None, None)?;
            articles.append(&mut tag_articles);
            enclosures.append(&mut tag_enclosures);
            taggings.append(&mut tag_taggings);
        }

        // get unread articles
        let (mut unread_articles, mut unread_enclosures, mut unread_taggings) =
            self.get_articles(&tag_all, Some(200), None, Some(true), None)?;
        articles.append(&mut unread_articles);
        enclosures.append(&mut unread_enclosures);
        taggings.append(&mut unread_taggings);

        Ok(SyncResult {
            feeds: Feedly::vec_to_option(feeds),
            categories: Feedly::vec_to_option(categories),
            mappings: Feedly::vec_to_option(mappings),
            tags: Feedly::vec_to_option(tags),
            taggings: Feedly::vec_to_option(taggings),
            headlines: None,
            articles: Feedly::vec_to_option(articles),
            enclosures: Feedly::vec_to_option(enclosures),
        })
    }

    fn sync(&mut self, max_count: u32, last_sync: NaiveDateTime) -> FeedApiResult<SyncResult> {
        let _ = max_count;
        let tag_all = self.api.category_all().context(FeedApiErrorKind::Api)?;

        let categories = self.api.get_categories().context(FeedApiErrorKind::Api)?;
        let categories = Feedly::convert_category_vec(categories);

        let feeds = self.api.get_subsriptions().context(FeedApiErrorKind::Api)?;
        let (feeds, mappings) = Feedly::convert_subscription_vec(feeds);

        let tags = self.api.get_tags().context(FeedApiErrorKind::Api)?;
        let tags = Feedly::convert_tag_vec(tags);

        let (articles, enclosures, taggings) = self.get_articles(
            &tag_all,
            Some(200),
            None,
            None,
            Some(last_sync.timestamp() as u64),
        )?;

        Ok(SyncResult {
            feeds: Feedly::vec_to_option(feeds),
            categories: Feedly::vec_to_option(categories),
            mappings: Feedly::vec_to_option(mappings),
            tags: Feedly::vec_to_option(tags),
            taggings: Feedly::vec_to_option(taggings),
            headlines: None,
            articles: Feedly::vec_to_option(articles),
            enclosures: Feedly::vec_to_option(enclosures),
        })
    }

    fn unread_count(&mut self) -> FeedApiResult<u32> {
        let counts = self.api.get_unread_counts().context(FeedApiErrorKind::Api)?;
        if let Some(unread_all) = counts
            .unreadcounts
            .iter()
            .find(|ref x| x.id.contains("category/global.all"))
        {
            return Ok(unread_all.count);
        }

        Err(FeedApiErrorKind::Unknown)?
    }

    fn set_article_read(
        &mut self,
        articles: &Vec<ArticleID>,
        read: models::Read,
    ) -> FeedApiResult<()> {
        let string_vec: Vec<&str> = articles.iter().map(|ref x| x.to_str()).collect();
        match read {
            models::Read::Read => self
                .api
                .mark_entries_read(string_vec)
                .context(FeedApiErrorKind::Api)?,
            models::Read::Unread => self
                .api
                .mark_entries_unread(string_vec)
                .context(FeedApiErrorKind::Api)?,
        };

        Ok(())
    }

    fn set_article_marked(
        &mut self,
        articles: &Vec<ArticleID>,
        marked: models::Marked,
    ) -> FeedApiResult<()> {
        let string_vec: Vec<&str> = articles.iter().map(|ref x| x.to_str()).collect();
        match marked {
            models::Marked::Marked => self
                .api
                .mark_entries_saved(string_vec)
                .context(FeedApiErrorKind::Api)?,
            models::Marked::Unmarked => self
                .api
                .mark_entries_unsaved(string_vec)
                .context(FeedApiErrorKind::Api)?,
        };

        Ok(())
    }

    fn set_feed_read(&mut self, feeds: &Vec<FeedID>) -> FeedApiResult<()> {
        let string_vec: Vec<&str> = feeds.iter().map(|ref x| x.to_str()).collect();
        self.api
            .mark_feeds_read(string_vec)
            .context(FeedApiErrorKind::Api)?;
        Ok(())
    }

    fn set_category_read(&mut self, categories: &Vec<CategoryID>) -> FeedApiResult<()> {
        let string_vec: Vec<&str> = categories.iter().map(|ref x| x.to_str()).collect();
        self.api
            .mark_categories_read(string_vec)
            .context(FeedApiErrorKind::Api)?;
        Ok(())
    }

    fn set_tag_read(&mut self, tags: &Vec<TagID>) -> FeedApiResult<()> {
        let string_vec: Vec<&str> = tags.iter().map(|ref x| x.to_str()).collect();
        self.api
            .mark_tags_read(string_vec)
            .context(FeedApiErrorKind::Api)?;
        Ok(())
    }

    fn set_all_read(&mut self) -> FeedApiResult<()> {
        let all = self.api.category_all().context(FeedApiErrorKind::Api)?;
        let vec: Vec<&str> = vec![&all];
        self.api
            .mark_categories_read(vec)
            .context(FeedApiErrorKind::Api)?;
        Ok(())
    }

    fn add_feed(
        &mut self,
        url: &Url,
        title: Option<&str>,
        category: Option<&CategoryID>,
    ) -> FeedApiResult<Feed> {
        let feed_id = FeedlyApi::gernerate_feed_id(url.get());

        let feed = SubscriptionInput {
            id: feed_id.clone(),
            title: match title.to_owned() {
                Some(title) => Some(title.to_owned()),
                None => None,
            },
            categories: match category {
                Some(category_id) => {
                    let category = FeedlyCategory {
                        id: category_id.to_string(),
                        label: None,
                        description: None,
                    };
                    Some(vec![category])
                }
                None => None,
            },
        };
        self.api
            .add_subscription(feed)
            .context(FeedApiErrorKind::Api)?;

        let feed_id = FeedID::new(&feed_id);
        let feed = match util::feed_parser::download_and_parse_feed(&url, &feed_id, title, None) {
            Ok(feed) => feed,
            Err(_) => {
                // parsing went wrong -> remove feed from feedly account and return the error
                self.remove_feed(&feed_id)?;
                return Err(FeedApiErrorKind::ParseFeed)?;
            }
        };
        Ok(feed)
    }

    fn remove_feed(&mut self, id: &FeedID) -> FeedApiResult<()> {
        self.api
            .delete_subscription(id.to_str())
            .context(FeedApiErrorKind::Api)?;
        Ok(())
    }

    fn move_feed(
        &mut self,
        feed_id: &FeedID,
        from: &CategoryID,
        to: &CategoryID,
    ) -> FeedApiResult<()> {
        let mappings = self
            .portal
            .get_mappings()
            .context(FeedApiErrorKind::Portal)?;
        // all categories for feed_id except category 'from'
        let mut categories: Vec<FeedlyCategory> = mappings
            .into_iter()
            .filter(|mapping| &mapping.feed_id == feed_id && &mapping.category_id != from)
            .map(|mapping| FeedlyCategory {
                id: mapping.category_id.to_string(),
                label: None,
                description: None,
            })
            .collect();

        // add category 'to'
        categories.push(FeedlyCategory {
            id: to.to_string(),
            label: None,
            description: None,
        });
        let feed = SubscriptionInput {
            id: feed_id.to_string(),
            title: None,
            categories: Some(categories),
        };
        self.api
            .add_subscription(feed)
            .context(FeedApiErrorKind::Api)?;
        Ok(())
    }

    fn rename_feed(&mut self, feed_id: &FeedID, new_title: &str) -> FeedApiResult<FeedID> {
        let feed = SubscriptionInput {
            id: feed_id.to_string(),
            title: Some(new_title.to_owned()),
            categories: None,
        };
        self.api
            .add_subscription(feed)
            .context(FeedApiErrorKind::Api)?;
        Ok(feed_id.clone())
    }

    fn add_category(
        &mut self,
        title: &str,
        parent: Option<&CategoryID>,
    ) -> FeedApiResult<CategoryID> {
        if parent.is_some() {
            Err(FeedApiErrorKind::Unsupported)?
        }

        // only generate id
        // useing id as if it would exist will create category
        let category_id = self
            .api
            .generate_category_id(&title)
            .context(FeedApiErrorKind::Api)?;
        Ok(CategoryID::new(&category_id))
    }

    fn remove_category(&mut self, id: &CategoryID, remove_children: bool) -> FeedApiResult<()> {
        if remove_children {
            let mappings = self
                .portal
                .get_mappings()
                .context(FeedApiErrorKind::Portal)?;

            let updated_subscriptions = mappings
                .iter()
                .filter(|m| &m.category_id == id)
                .map(|m| SubscriptionInput {
                    id: m.feed_id.to_string(),
                    title: None,
                    categories: {
                        let categories = mappings
                            .iter()
                            .filter(|m2| m2.feed_id == m.feed_id && &m2.category_id != id)
                            .map(|m3| FeedlyCategory {
                                id: m3.category_id.to_string(),
                                label: None,
                                description: None,
                            })
                            .collect::<Vec<FeedlyCategory>>();
                        Some(categories)
                    },
                })
                .collect::<Vec<SubscriptionInput>>();

            self.api
                .update_subscriptions(updated_subscriptions)
                .context(FeedApiErrorKind::Api)?;
        }
        self.api
            .delete_category(id.to_str())
            .context(FeedApiErrorKind::Api)?;
        Ok(())
    }

    fn rename_category(&mut self, id: &CategoryID, new_title: &str) -> FeedApiResult<CategoryID> {
        let new_id = self
            .api
            .generate_category_id(&new_title)
            .context(FeedApiErrorKind::Api)?;
        self.api
            .update_category(id.to_str(), new_title)
            .context(FeedApiErrorKind::Api)?;
        Ok(CategoryID::new(&new_id))
    }

    fn move_category(&mut self, id: &CategoryID, parent: &CategoryID) -> FeedApiResult<()> {
        // mute unused warnings
        let _ = id;
        let _ = parent;
        Err(FeedApiErrorKind::Unsupported)?
    }

    fn import_opml(&mut self, opml: &str) -> FeedApiResult<()> {
        self.api.import_opml(opml).context(FeedApiErrorKind::Api)?;
        Ok(())
    }

    fn add_tag(&mut self, title: &str) -> FeedApiResult<TagID> {
        let id = self
            .api
            .generate_tag_id(title)
            .context(FeedApiErrorKind::Api)?;
        Ok(TagID::new(&id))
    }

    fn remove_tag(&mut self, id: &TagID) -> FeedApiResult<()> {
        self.api
            .delete_tags(vec![id.to_str()])
            .context(FeedApiErrorKind::Api)?;
        Ok(())
    }

    fn rename_tag(&mut self, id: &TagID, new_title: &str) -> FeedApiResult<TagID> {
        let new_id = self
            .api
            .generate_tag_id(&new_title)
            .context(FeedApiErrorKind::Api)?;
        self.api
            .update_tag(id.to_str(), new_title)
            .context(FeedApiErrorKind::Api)?;
        Ok(TagID::new(&new_id))
    }

    fn tag_article(&mut self, article_id: &ArticleID, tag_id: &TagID) -> FeedApiResult<()> {
        self.api
            .tag_entry(article_id.to_str(), vec![tag_id.to_str()])
            .context(FeedApiErrorKind::Api)?;
        Ok(())
    }

    fn untag_article(&mut self, article_id: &ArticleID, tag_id: &TagID) -> FeedApiResult<()> {
        self.api
            .untag_entries(vec![article_id.to_str()], vec![tag_id.to_str()])
            .context(FeedApiErrorKind::Api)?;
        Ok(())
    }
}
