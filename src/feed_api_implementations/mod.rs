pub mod feedly;

use self::feedly::metadata::FeedlyMetadata;
use feed_api::ApiMetadata;
use models::PluginID;
use std::collections::hash_map::HashMap;

pub fn list() -> HashMap<PluginID, Box<ApiMetadata>> {
    let mut h: HashMap<PluginID, Box<ApiMetadata>> = HashMap::new();

    h.insert(FeedlyMetadata.id(), Box::new(FeedlyMetadata));

    h
}
