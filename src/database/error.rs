use diesel;
use failure::{Backtrace, Context, Error, Fail};
use std::fmt;

#[derive(Debug)]
pub struct DataBaseError {
    inner: Context<DataBaseErrorKind>,
}

#[derive(Copy, Clone, Eq, PartialEq, Debug, Fail)]
pub enum DataBaseErrorKind {
    #[fail(display = "Error connecting to the database")]
    Open,
    #[fail(display = "Invalid data directory")]
    InvalidPath,
    #[fail(display = "Error updating data")]
    Update,
    #[fail(display = "Error migrating db schema")]
    Migration,
    #[fail(display = "Error deleting data")]
    Delete,
    #[fail(display = "Error retrieving data")]
    Select,
    #[fail(display = "Error inserting data")]
    Insert,
    #[fail(display = "Error setting options")]
    Options,
    #[fail(display = "Unknown Error")]
    Unknown,
}

impl Fail for DataBaseError {
    fn cause(&self) -> Option<&Fail> {
        self.inner.cause()
    }

    fn backtrace(&self) -> Option<&Backtrace> {
        self.inner.backtrace()
    }
}

impl fmt::Display for DataBaseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Display::fmt(&self.inner, f)
    }
}

// impl DataBaseError {
//     pub fn kind(&self) -> DataBaseErrorKind {
//         *self.inner.get_context()
//     }
// }

impl From<DataBaseErrorKind> for DataBaseError {
    fn from(kind: DataBaseErrorKind) -> DataBaseError {
        DataBaseError {
            inner: Context::new(kind),
        }
    }
}

impl From<Context<DataBaseErrorKind>> for DataBaseError {
    fn from(inner: Context<DataBaseErrorKind>) -> DataBaseError {
        DataBaseError { inner: inner }
    }
}

impl From<Error> for DataBaseError {
    fn from(_: Error) -> DataBaseError {
        DataBaseError {
            inner: Context::new(DataBaseErrorKind::Unknown),
        }
    }
}

impl From<diesel::result::Error> for DataBaseError {
    fn from(_: diesel::result::Error) -> DataBaseError {
        DataBaseError {
            inner: Context::new(DataBaseErrorKind::Unknown),
        }
    }
}
