mod error;

use self::error::{DataBaseError, DataBaseErrorKind};
use chrono::NaiveDateTime;
use diesel;
use diesel::prelude::*;
use diesel::sqlite::SqliteConnection;
use failure::ResultExt;
use models::{
    Article, ArticleID, Category, CategoryID, Enclosure, Feed, FeedID, FeedMapping, Headline,
    Marked, Read, SyncResult, Tag, TagID, Tagging,
};
use schema::{articles, categories, enclosures, feed_mapping, feeds, taggings, tags};
use std::fs;
use std::path::{Path, PathBuf};

pub enum Order {
    NewestFirst,
    OldestFirst,
}

type DataBaseResult<T> = Result<T, DataBaseError>;

pub struct DataBase {
    connection: SqliteConnection,
}

embed_migrations!("./migrations");

impl DataBase {
    fn new_with_file_name(data_dir: &Path, file_name: &str) -> DataBaseResult<DataBase> {
        if !data_dir.is_dir() {
            return Err(DataBaseErrorKind::InvalidPath)?;
        }

        fs::create_dir_all(&data_dir).context(DataBaseErrorKind::InvalidPath)?;
        let database_url = data_dir.join(file_name);
        let database_url = match database_url.to_str() {
            Some(url) => url,
            None => return Err(DataBaseErrorKind::InvalidPath)?,
        };

        let db = DataBase {
            connection: DataBase::establish_connection(database_url)?,
        };
        Ok(db)
    }

    pub fn new(data_dir: &PathBuf) -> DataBaseResult<DataBase> {
        DataBase::new_with_file_name(data_dir, "database.sqlite")
    }

    fn establish_connection(database_url: &str) -> DataBaseResult<SqliteConnection> {
        let connection =
            SqliteConnection::establish(&database_url).context(DataBaseErrorKind::Open)?;
        diesel::sql_query("PRAGMA foreign_key=ON")
            .execute(&connection)
            .context(DataBaseErrorKind::Options)?;
        embedded_migrations::run(&connection).context(DataBaseErrorKind::Migration)?;
        Ok(connection)
    }

    pub fn write_tags(&self, tags: &Vec<Tag>) -> Result<(), DataBaseError> {
        self.connection.transaction::<(), DataBaseError, _>(|| {
            // delete old tags
            diesel::delete(tags::table)
                .execute(&self.connection)
                .context(DataBaseErrorKind::Delete)?;

            // write new tags
            diesel::replace_into(tags::table)
                .values(tags)
                .execute(&self.connection)
                .context(DataBaseErrorKind::Insert)?;
            Ok(())
        })
    }

    pub fn insert_tag(&self, tag: &Tag) -> Result<(), DataBaseError> {
        diesel::replace_into(tags::table)
            .values(tag)
            .execute(&self.connection)
            .context(DataBaseErrorKind::Insert)?;

        Ok(())
    }

    pub fn insert_tags(&self, tags: &Vec<Tag>) -> Result<(), DataBaseError> {
        diesel::replace_into(tags::table)
            .values(tags)
            .execute(&self.connection)
            .context(DataBaseErrorKind::Insert)?;

        Ok(())
    }

    pub fn read_tags(&self) -> DataBaseResult<Vec<Tag>> {
        let tags = tags::table
            .load(&self.connection)
            .context(DataBaseErrorKind::Select)?;
        Ok(tags)
    }

    pub fn drop_tag(&self, tag: &Tag) -> Result<(), DataBaseError> {
        diesel::delete(tags::table)
            .filter(tags::tag_id.eq(&tag.tag_id))
            .execute(&self.connection)
            .context(DataBaseErrorKind::Delete)?;
        Ok(())
    }

    pub fn set_tag_read(&self, tags: &Vec<TagID>) -> DataBaseResult<()> {
        self.connection.transaction::<(), DataBaseError, _>(|| {
            let articles: Vec<ArticleID> = taggings::table
                .filter(taggings::tag_id.eq_any(tags))
                .select(taggings::article_id)
                .load(&self.connection)
                .context(DataBaseErrorKind::Select)?;
            self.set_article_read(&articles, Read::Read)?;
            Ok(())
        })
    }

    pub fn insert_tagging(&self, tagging: &Tagging) -> DataBaseResult<()> {
        diesel::replace_into(taggings::table)
            .values(tagging)
            .execute(&self.connection)
            .context(DataBaseErrorKind::Insert)?;

        Ok(())
    }

    pub fn insert_taggings(&self, taggings: &Vec<Tagging>) -> DataBaseResult<()> {
        diesel::replace_into(taggings::table)
            .values(taggings)
            .execute(&self.connection)
            .context(DataBaseErrorKind::Insert)?;

        Ok(())
    }

    pub fn read_taggings(
        &self,
        article: Option<&ArticleID>,
        tag: Option<&TagID>,
    ) -> DataBaseResult<Vec<Tagging>> {
        let mut query = taggings::table.into_boxed();

        if article.is_some() && tag.is_some() {
            warn!("querying tagging by article AND tag might not be very useful");
        }

        if let Some(article) = article {
            query = query.filter(taggings::article_id.eq(article.to_string()));
        }

        if let Some(tag) = tag {
            query = query.filter(taggings::tag_id.eq(tag.to_string()));
        }

        let taggings = query
            .load(&self.connection)
            .context(DataBaseErrorKind::Select)?;
        Ok(taggings)
    }

    pub fn drop_tagging(&self, tagging: &Tagging) -> DataBaseResult<()> {
        diesel::delete(taggings::table)
            .filter(taggings::tag_id.eq(&tagging.tag_id))
            .filter(taggings::article_id.eq(&tagging.article_id))
            .execute(&self.connection)
            .context(DataBaseErrorKind::Delete)?;
        Ok(())
    }

    pub fn drop_taggings_of_tag(&self, tag: &Tag) -> DataBaseResult<()> {
        diesel::delete(taggings::table)
            .filter(taggings::tag_id.eq(&tag.tag_id))
            .execute(&self.connection)
            .context(DataBaseErrorKind::Delete)?;
        Ok(())
    }

    pub fn drop_taggings_of_article(&self, article: &ArticleID) -> DataBaseResult<()> {
        diesel::delete(taggings::table)
            .filter(taggings::article_id.eq(article))
            .execute(&self.connection)
            .context(DataBaseErrorKind::Delete)?;
        Ok(())
    }

    pub fn drop_taggings_of_articles(&self, articles: &Vec<ArticleID>) -> DataBaseResult<()> {
        diesel::delete(taggings::table)
            .filter(taggings::article_id.eq_any(articles))
            .execute(&self.connection)
            .context(DataBaseErrorKind::Delete)?;
        Ok(())
    }

    pub fn write_categories(&self, categories: &Vec<Category>) -> DataBaseResult<()> {
        self.connection.transaction::<(), DataBaseError, _>(|| {
            // delete old categories
            diesel::delete(categories::table)
                .execute(&self.connection)
                .context(DataBaseErrorKind::Delete)?;

            // write new categories
            diesel::replace_into(categories::table)
                .values(categories)
                .execute(&self.connection)
                .context(DataBaseErrorKind::Insert)?;

            Ok(())
        })
    }

    pub fn insert_category(&self, category: &Category) -> DataBaseResult<()> {
        diesel::replace_into(categories::table)
            .values(category)
            .execute(&self.connection)
            .context(DataBaseErrorKind::Insert)?;

        Ok(())
    }

    pub fn insert_categories(&self, categories: &Vec<Category>) -> DataBaseResult<()> {
        diesel::replace_into(categories::table)
            .values(categories)
            .execute(&self.connection)
            .context(DataBaseErrorKind::Insert)?;

        Ok(())
    }

    pub fn read_categories(&self) -> DataBaseResult<Vec<Category>> {
        let categories = categories::table
            .load(&self.connection)
            .context(DataBaseErrorKind::Select)?;
        Ok(categories)
    }

    pub fn drop_category(&self, category: &Category) -> DataBaseResult<()> {
        self.connection.transaction::<(), DataBaseError, _>(|| {
            diesel::delete(feed_mapping::table)
                .filter(feed_mapping::category_id.eq(&category.category_id))
                .execute(&self.connection)
                .context(DataBaseErrorKind::Delete)?;

            diesel::delete(categories::table)
                .filter(categories::category_id.eq(&category.category_id))
                .execute(&self.connection)
                .context(DataBaseErrorKind::Delete)?;
            Ok(())
        })
    }

    pub fn set_category_read(&self, categories: &Vec<CategoryID>) -> DataBaseResult<()> {
        self.connection.transaction::<(), DataBaseError, _>(|| {
            let feeds: Vec<FeedID> = feed_mapping::table
                .filter(feed_mapping::category_id.eq_any(categories))
                .select(feed_mapping::feed_id)
                .load(&self.connection)
                .context(DataBaseErrorKind::Select)?;

            self.set_feed_read(&feeds)?;
            Ok(())
        })
    }

    pub fn write_feeds(&self, feeds: &Vec<Feed>) -> DataBaseResult<()> {
        self.connection.transaction::<(), DataBaseError, _>(|| {
            // delete old feeds
            diesel::delete(feeds::table)
                .execute(&self.connection)
                .context(DataBaseErrorKind::Delete)?;

            // write new feeds
            diesel::replace_into(feeds::table)
                .values(feeds)
                .execute(&self.connection)
                .context(DataBaseErrorKind::Insert)?;

            Ok(())
        })
    }

    pub fn insert_feed(&self, feed: &Feed) -> DataBaseResult<()> {
        diesel::replace_into(feeds::table)
            .values(feed)
            .execute(&self.connection)
            .context(DataBaseErrorKind::Insert)?;

        Ok(())
    }

    pub fn insert_feeds(&self, feeds: &Vec<Feed>) -> DataBaseResult<()> {
        diesel::replace_into(feeds::table)
            .values(feeds)
            .execute(&self.connection)
            .context(DataBaseErrorKind::Insert)?;

        Ok(())
    }

    pub fn read_feeds(&self) -> DataBaseResult<Vec<Feed>> {
        let feeds = feeds::table
            .load(&self.connection)
            .context(DataBaseErrorKind::Select)?;
        Ok(feeds)
    }

    pub fn drop_feed(&self, feed_id: &FeedID) -> DataBaseResult<()> {
        self.connection.transaction::<(), DataBaseError, _>(|| {
            diesel::delete(feed_mapping::table)
                .filter(feed_mapping::feed_id.eq(feed_id))
                .execute(&self.connection)
                .context(DataBaseErrorKind::Delete)?;

            diesel::delete(feeds::table)
                .filter(feeds::feed_id.eq(feed_id))
                .execute(&self.connection)
                .context(DataBaseErrorKind::Delete)?;

            Ok(())
        })
    }

    pub fn set_feed_read(&self, feeds: &Vec<FeedID>) -> DataBaseResult<()> {
        diesel::update(articles::table)
            .filter(articles::feed_id.eq_any(feeds))
            .set(articles::unread.eq(Read::Read.to_int()))
            .execute(&self.connection)
            .context(DataBaseErrorKind::Update)?;
        Ok(())
    }

    pub fn write_mappings(&self, mappings: &Vec<FeedMapping>) -> DataBaseResult<()> {
        self.connection.transaction::<(), DataBaseError, _>(|| {
            // delete old mappings
            diesel::delete(feed_mapping::table)
                .execute(&self.connection)
                .context(DataBaseErrorKind::Delete)?;

            // write new mappings
            diesel::replace_into(feed_mapping::table)
                .values(mappings)
                .execute(&self.connection)
                .context(DataBaseErrorKind::Insert)?;

            Ok(())
        })
    }

    pub fn read_mappings(
        &self,
        feed: Option<&FeedID>,
        category: Option<&CategoryID>,
    ) -> DataBaseResult<Vec<FeedMapping>> {
        let mut query = feed_mapping::table.into_boxed();

        if feed.is_some() && category.is_some() {
            warn!("querying mapping by feed AND category might not be very useful");
        }

        if let Some(feed) = feed {
            query = query.filter(feed_mapping::feed_id.eq(feed.to_string()));
        }

        if let Some(category) = category {
            query = query.filter(feed_mapping::category_id.eq(category.to_string()));
        }

        let mappings = query
            .load(&self.connection)
            .context(DataBaseErrorKind::Select)?;
        Ok(mappings)
    }

    pub fn drop_mapping(&self, mapping: &FeedMapping) -> DataBaseResult<()> {
        diesel::delete(feed_mapping::table)
            .filter(feed_mapping::feed_id.eq(&mapping.feed_id))
            .filter(feed_mapping::category_id.eq(&mapping.category_id))
            .execute(&self.connection)
            .context(DataBaseErrorKind::Delete)?;
        Ok(())
    }

    pub fn insert_mapping(&self, mapping: &FeedMapping) -> DataBaseResult<()> {
        diesel::replace_into(feed_mapping::table)
            .values(mapping)
            .execute(&self.connection)
            .context(DataBaseErrorKind::Insert)?;

        Ok(())
    }

    pub fn insert_mappings(&self, mappings: &Vec<FeedMapping>) -> DataBaseResult<()> {
        diesel::replace_into(feed_mapping::table)
            .values(mappings)
            .execute(&self.connection)
            .context(DataBaseErrorKind::Insert)?;

        Ok(())
    }

    pub fn write_articles(&self, articles: &Vec<Article>) -> DataBaseResult<()> {
        diesel::replace_into(articles::table)
            .values(articles)
            .execute(&self.connection)
            .context(DataBaseErrorKind::Insert)?;

        Ok(())
    }

    pub fn read_articles(
        &self,
        limit: Option<i64>,
        offset: Option<i64>,
        order: Option<Order>,
        unread: Option<Read>,
        marked: Option<Marked>,
        feed: Option<&FeedID>,
        category: Option<&CategoryID>,
        ids: Option<Vec<ArticleID>>,
        newer_than: Option<NaiveDateTime>,
        older_than: Option<NaiveDateTime>,
    ) -> DataBaseResult<Vec<Article>> {
        let limit = match limit {
            Some(limit) => limit,
            None => 20,
        };

        let offset = match offset {
            Some(offset) => offset,
            None => 0,
        };

        let mut query = articles::table.offset(offset).limit(limit).into_boxed();

        if let Some(order) = order {
            match order {
                Order::NewestFirst => query = query.order(articles::timestamp.asc()),
                Order::OldestFirst => query = query.order(articles::timestamp.desc()),
            }
        }

        if let Some(unread) = unread {
            query = query.filter(articles::unread.eq(unread.to_int()));
        }

        if let Some(marked) = marked {
            query = query.filter(articles::marked.eq(marked.to_int()));
        }

        if feed.is_some() && category.is_some() {
            warn!("querying article by feed AND category might not be very useful");
        }

        if let Some(feed) = feed {
            query = query.filter(articles::feed_id.eq(feed.to_string()));
        }

        if let Some(category) = category {
            let feed_ids_in_category = feed_mapping::table
                .select(feed_mapping::feed_id)
                .filter(feed_mapping::category_id.eq(category));

            query = query.filter(articles::feed_id.eq_any(feed_ids_in_category));
        }

        if let Some(newer_than) = newer_than {
            if let Some(older_than) = older_than {
                if older_than > newer_than {
                    warn!(
                        "Impossible constraint: Older than '{}' and newer than '{}'",
                        older_than, newer_than
                    );
                }
            }
        }

        if let Some(ids) = ids {
            query = query.filter(articles::article_id.eq_any(ids));
        }

        if let Some(newer_than) = newer_than {
            query = query.filter(articles::timestamp.gt(newer_than));
        }

        if let Some(older_than) = older_than {
            query = query.filter(articles::timestamp.lt(older_than));
        }

        let articles = query
            .load(&self.connection)
            .context(DataBaseErrorKind::Select)?;
        Ok(articles)
    }

    pub fn drop_article(&self, article: &Article) -> DataBaseResult<()> {
        self.connection.transaction::<(), DataBaseError, _>(|| {
            self.drop_taggings_of_article(&article.article_id)?;
            self.drop_enclosures_of_article(&article.article_id)?;

            diesel::delete(articles::table)
                .filter(articles::article_id.eq(&article.article_id))
                .execute(&self.connection)
                .context(DataBaseErrorKind::Delete)?;
            Ok(())
        })
    }

    pub fn drop_articles(&self, articles: &Vec<ArticleID>) -> DataBaseResult<()> {
        self.connection.transaction::<(), DataBaseError, _>(|| {
            self.drop_taggings_of_articles(&articles)?;
            self.drop_enclosures_of_articles(&articles)?;

            diesel::delete(articles::table)
                .filter(articles::article_id.eq_any(articles))
                .execute(&self.connection)
                .context(DataBaseErrorKind::Delete)?;
            Ok(())
        })
    }

    pub fn drop_articles_older_than(&self, older_than: NaiveDateTime) -> DataBaseResult<()> {
        self.connection.transaction::<(), DataBaseError, _>(|| {
            let article_ids: Vec<ArticleID> = self
                .read_articles(
                    None,             // limit
                    None,             // offset
                    None,             // order
                    None,             // unread
                    None,             // marked
                    None,             // feed
                    None,             // category
                    None,             // article ids
                    None,             // newer_than
                    Some(older_than), // older_than
                )?
                .iter()
                .map(|a| a.article_id.clone())
                .collect();

            self.drop_articles(&article_ids)?;
            Ok(())
        })
    }

    pub fn set_article_read(&self, articles: &Vec<ArticleID>, read: Read) -> DataBaseResult<()> {
        diesel::update(articles::table)
            .filter(articles::article_id.eq_any(articles))
            .set(articles::unread.eq(read.to_int()))
            .execute(&self.connection)
            .context(DataBaseErrorKind::Update)?;
        Ok(())
    }

    pub fn set_all_read(&self) -> DataBaseResult<()> {
        diesel::update(articles::table)
            .set(articles::unread.eq(Read::Read.to_int()))
            .execute(&self.connection)
            .context(DataBaseErrorKind::Update)?;
        Ok(())
    }

    pub fn set_article_marked(
        &self,
        articles: &Vec<ArticleID>,
        marked: Marked,
    ) -> DataBaseResult<()> {
        diesel::update(articles::table)
            .filter(articles::article_id.eq_any(articles))
            .set(articles::marked.eq(marked.to_int()))
            .execute(&self.connection)
            .context(DataBaseErrorKind::Update)?;
        Ok(())
    }

    pub fn read_enclosures(&self, article: &ArticleID) -> DataBaseResult<Vec<Enclosure>> {
        let enclosures = enclosures::table
            .filter(enclosures::article_id.eq(article))
            .load(&self.connection)
            .context(DataBaseErrorKind::Select)?;

        Ok(enclosures)
    }

    pub fn write_enclosures(&self, enclosures: &Vec<Enclosure>) -> DataBaseResult<()> {
        diesel::replace_into(enclosures::table)
            .values(enclosures)
            .execute(&self.connection)
            .context(DataBaseErrorKind::Insert)?;
        Ok(())
    }

    pub fn drop_enclosures_of_article(&self, article: &ArticleID) -> DataBaseResult<()> {
        diesel::delete(enclosures::table)
            .filter(enclosures::article_id.eq(&article))
            .execute(&self.connection)
            .context(DataBaseErrorKind::Delete)?;
        Ok(())
    }

    pub fn drop_enclosures_of_articles(&self, articles: &Vec<ArticleID>) -> DataBaseResult<()> {
        diesel::delete(enclosures::table)
            .filter(enclosures::article_id.eq_any(articles))
            .execute(&self.connection)
            .context(DataBaseErrorKind::Delete)?;
        Ok(())
    }

    pub fn insert_headlines(&self, headlines: &Vec<Headline>) -> DataBaseResult<()> {
        self.connection.transaction::<(), DataBaseError, _>(|| {
            for headline in headlines {
                diesel::update(articles::table)
                    .filter(articles::article_id.eq(&headline.article_id))
                    .set((
                        articles::unread.eq(headline.unread.to_int()),
                        articles::marked.eq(headline.marked.to_int()),
                    ))
                    .execute(&self.connection)
                    .context(DataBaseErrorKind::Update)?;
            }
            Ok(())
        })
    }

    pub fn write_sync_result(&self, result: SyncResult) -> DataBaseResult<()> {
        self.connection.transaction::<(), DataBaseError, _>(|| {
            if let Some(categories) = result.categories {
                self.write_categories(&categories)?;
            }

            if let Some(feeds) = result.feeds {
                self.write_feeds(&feeds)?;
            }

            if let Some(mappings) = result.mappings {
                self.write_mappings(&mappings)?;
            }

            if let Some(tags) = result.tags {
                self.write_tags(&tags)?;
            }

            if let Some(articles) = result.articles {
                self.write_articles(&articles)?;
            }

            if let Some(headlines) = result.headlines {
                self.insert_headlines(&headlines)?;
            }

            if let Some(taggings) = result.taggings {
                self.insert_taggings(&taggings)?;
            }

            if let Some(enclosures) = result.enclosures {
                self.write_enclosures(&enclosures)?;
            }

            Err(DataBaseErrorKind::Unknown)?
        })
    }
}

#[cfg(test)]
mod tests {
    use chrono::Local;
    use database::DataBase;
    use models::{
        Article, ArticleID, Category, CategoryID, Direction, Feed, FeedID, FeedMapping, Marked,
        Read, Tag, TagID, Tagging, Url, NEWSFLASH_TOPLEVEL,
    };
    use tempfile::{tempdir, TempDir};

    fn setup_db(database_url: &TempDir) -> DataBase {
        DataBase::new_with_file_name(database_url.path(), "test.sqlite").unwrap()
    }

    fn setup_tags() -> Vec<Tag> {
        vec![
            Tag {
                tag_id: TagID::new("tag_1"),
                label: String::from("tag_1_label"),
                color: Some(1),
            },
            Tag {
                tag_id: TagID::new("tag_2"),
                label: String::from("tag_2_label"),
                color: Some(2),
            },
        ]
    }

    fn setup_taggings() -> Vec<Tagging> {
        vec![
            Tagging {
                article_id: ArticleID::new("article_1"),
                tag_id: TagID::new("tag_1"),
            },
            Tagging {
                article_id: ArticleID::new("article_2"),
                tag_id: TagID::new("tag_2"),
            },
        ]
    }

    fn setup_categories() -> Vec<Category> {
        vec![
            Category {
                category_id: CategoryID::new("category_1"),
                label: String::from("category_1_label"),
                sort_index: Some(1),
                parent: NEWSFLASH_TOPLEVEL.clone(),
            },
            Category {
                category_id: CategoryID::new("category_2"),
                label: String::from("category_2_label"),
                sort_index: Some(2),
                parent: NEWSFLASH_TOPLEVEL.clone(),
            },
        ]
    }

    fn setup_feeds() -> Vec<Feed> {
        vec![
            Feed {
                feed_id: FeedID::new("feed_1"),
                label: String::from("feed_1_label"),
                website: Some(Url::parse("http://feed-1.com").unwrap()),
                feed_url: Some(Url::parse("http://feed-1.com/rss").unwrap()),
                icon_url: Some(Url::parse("http://feed-1.com/fav.ico").unwrap()),
                sort_index: Some(1),
            },
            Feed {
                feed_id: FeedID::new("feed_2"),
                label: String::from("feed_2_label"),
                website: Some(Url::parse("http://feed-2.com").unwrap()),
                feed_url: Some(Url::parse("http://feed-2.com/rss").unwrap()),
                icon_url: Some(Url::parse("http://feed-2.com/fav.ico").unwrap()),
                sort_index: Some(2),
            },
        ]
    }

    fn setup_mappings() -> Vec<FeedMapping> {
        vec![
            FeedMapping {
                feed_id: FeedID::new("feed_1"),
                category_id: CategoryID::new("category_1"),
            },
            FeedMapping {
                feed_id: FeedID::new("feed_2"),
                category_id: CategoryID::new("category_2"),
            },
        ]
    }

    fn setup_articles() -> Vec<Article> {
        vec![
            Article {
                article_id: ArticleID::new("article_1"),
                title: Some(String::from("article_1_title")),
                author: Some(String::from("article_1_author")),
                feed_id: FeedID::new("feed_1"),
                url: None,
                date: Local::now().naive_local(),
                html: None,
                summary: None,
                direction: Some(Direction::LeftToRight),
                unread: Read::Unread,
                marked: Marked::Unmarked,
            },
            Article {
                article_id: ArticleID::new("article_2"),
                title: Some(String::from("article_2_title")),
                author: Some(String::from("article_2_author")),
                feed_id: FeedID::new("feed_2"),
                url: None,
                date: Local::now().naive_local(),
                html: None,
                summary: None,
                direction: Some(Direction::LeftToRight),
                unread: Read::Unread,
                marked: Marked::Unmarked,
            },
        ]
    }

    #[test]
    fn write_read_tags() {
        let database_url = tempdir().unwrap();
        let db = setup_db(&database_url);
        let tags = setup_tags();
        db.write_tags(&tags).unwrap();
        let read_tags = db.read_tags().unwrap();

        assert_eq!(tags, read_tags);
    }

    #[test]
    fn write_read_taggings() {
        let database_url = tempdir().unwrap();
        let db = setup_db(&database_url);
        let taggings = setup_taggings();
        db.insert_taggings(&taggings).unwrap();
        let read_taggings = db.read_taggings(None, None).unwrap();

        assert_eq!(taggings, read_taggings);
    }

    #[test]
    fn write_read_categories() {
        let database_url = tempdir().unwrap();
        let db = setup_db(&database_url);
        let categories = setup_categories();
        db.write_categories(&categories).unwrap();
        let read_categories = db.read_categories().unwrap();

        assert_eq!(categories, read_categories);
    }

    #[test]
    fn write_read_feeds() {
        let database_url = tempdir().unwrap();
        let db = setup_db(&database_url);
        let feeds = setup_feeds();
        db.write_feeds(&feeds).unwrap();
        let read_feeds = db.read_feeds().unwrap();

        assert_eq!(feeds, read_feeds);
    }

    #[test]
    fn write_read_mappings() {
        let database_url = tempdir().unwrap();
        let db = setup_db(&database_url);
        let mappings = setup_mappings();
        db.write_mappings(&mappings).unwrap();
        let read_mappings = db.read_mappings(None, None).unwrap();

        assert_eq!(mappings, read_mappings);
    }

    #[test]
    fn write_read_articles() {
        let database_url = tempdir().unwrap();
        let db = setup_db(&database_url);
        let articles = setup_articles();
        db.write_articles(&articles).unwrap();
        let read_articles =
            db.read_articles(
                None, // limit
                None, // offset
                None, // order
                None, // unread
                None, // marked
                None, // feed
                None, // category
                None, // article ids
                None, // newer_than
                None, // older_than
            ).unwrap();

        assert_eq!(articles, read_articles);
    }
}
