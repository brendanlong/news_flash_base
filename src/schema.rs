table! {
    articles (article_id) {
        article_id -> Text,
        title -> Nullable<Text>,
        author -> Nullable<Text>,
        feed_id -> Text,
        url -> Nullable<Text>,
        timestamp -> Timestamp,
        html -> Nullable<Text>,
        summary -> Nullable<Text>,
        direction -> Nullable<Integer>,
        unread -> Integer,
        marked -> Integer,
    }
}

table! {
    categories (category_id) {
        category_id -> Text,
        label -> Text,
        order_id -> Nullable<Integer>,
        parent_id -> Text,
    }
}

table! {
    enclosures (article_id) {
        article_id -> Text,
        url -> Text,
        #[sql_name = "type"]
        type_ -> Integer,
    }
}

table! {
    feed_mapping (feed_id) {
        feed_id -> Text,
        category_id -> Text,
    }
}

table! {
    feeds (feed_id) {
        feed_id -> Text,
        label -> Text,
        website -> Nullable<Text>,
        feed_url -> Nullable<Text>,
        icon_url -> Nullable<Text>,
        order_id -> Nullable<Integer>,
    }
}

table! {
    taggings (article_id) {
        article_id -> Text,
        tag_id -> Text,
    }
}

table! {
    tags (tag_id) {
        tag_id -> Text,
        label -> Text,
        color -> Nullable<Integer>,
    }
}

joinable!(articles -> feeds (feed_id));
joinable!(enclosures -> articles (article_id));
joinable!(feed_mapping -> categories (category_id));
joinable!(feed_mapping -> feeds (feed_id));
joinable!(taggings -> articles (article_id));
joinable!(taggings -> tags (tag_id));

allow_tables_to_appear_in_same_query!(
    articles,
    categories,
    enclosures,
    feed_mapping,
    feeds,
    taggings,
    tags,
);
