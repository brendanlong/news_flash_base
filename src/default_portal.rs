use database::DataBase;
use failure::ResultExt;
use feed_api::portal::{Portal, PortalErrorKind, PortalResult};
use models::{Article, ArticleID, Category, Feed, FeedMapping, Headline, Tag};
use std::rc::Rc;

pub struct DefaultPortal {
    db: Rc<DataBase>,
}

impl DefaultPortal {
    pub fn new(db: Rc<DataBase>) -> DefaultPortal {
        DefaultPortal { db: db }
    }
}

impl Portal for DefaultPortal {
    fn get_headlines(&self, ids: Vec<ArticleID>) -> PortalResult<Vec<Headline>> {
        let articles = self
            .db
            .read_articles(
                None,
                None,
                None,
                None,
                None,
                None,
                None,
                Some(ids),
                None,
                None,
            )
            .context(PortalErrorKind::Unknown)?;
        let headlines = articles.iter().map(|a| Headline::from_article(a)).collect();
        Ok(headlines)
    }

    fn get_articles(&self, ids: Vec<ArticleID>) -> PortalResult<Vec<Article>> {
        let articles = self
            .db
            .read_articles(
                None,
                None,
                None,
                None,
                None,
                None,
                None,
                Some(ids),
                None,
                None,
            )
            .context(PortalErrorKind::Unknown)?;
        Ok(articles)
    }

    fn get_feeds(&self) -> PortalResult<Vec<Feed>> {
        let feeds = self.db.read_feeds().context(PortalErrorKind::Unknown)?;
        Ok(feeds)
    }

    fn get_categories(&self) -> PortalResult<Vec<Category>> {
        let categories = self.db.read_categories().context(PortalErrorKind::Unknown)?;
        Ok(categories)
    }

    fn get_mappings(&self) -> PortalResult<Vec<FeedMapping>> {
        let mappings = self
            .db
            .read_mappings(None, None)
            .context(PortalErrorKind::Unknown)?;
        Ok(mappings)
    }

    fn get_tags(&self) -> PortalResult<Vec<Tag>> {
        let tags = self.db.read_tags().context(PortalErrorKind::Unknown)?;
        Ok(tags)
    }
}
