mod error;

use self::error::{FeedParserError, FeedParserErrorKind};
use atom_syndication::Feed as AtomFeed;
use failure::ResultExt;
use jsonfeed;
use libxml::{parser::Parser, xpath::Context};
use models::{Feed, FeedID, Url};
use reqwest;
use rss::Channel as RssFeed;
use std::io::{BufReader, Read};
use std::str;

#[derive(Debug, PartialEq)]
enum ContentType {
    Rss,
    Atom,
    JsonFeed,
    Html,
}

pub fn download_and_parse_feed(
    url: &Url,
    id: &FeedID,
    title: Option<&str>,
    order_id: Option<i32>,
) -> Result<Feed, FeedParserError> {
    let mut result = reqwest::get(url.get()).context(FeedParserErrorKind::Http)?;
    if !result.status().is_success() {
        error!("Downloading feed failed: {}", url);
        return Err(FeedParserErrorKind::Http)?;
    }

    let content_type = check_content_type(result.headers().get::<reqwest::header::ContentType>());
    let mut result_buffer: Vec<u8> = Vec::new();
    result
        .read_to_end(&mut result_buffer)
        .map_err(|err| {
            error!("Reading response as string failed: {}", url);
            err
        })
        .context(FeedParserErrorKind::Http)?;

    if content_type == Some(ContentType::Html) {
        debug!("ContentType: Html -> trying to parse page for feed url");
        let reult_string = str::from_utf8(&result_buffer).context(FeedParserErrorKind::Utf8)?;
        if let Ok(feed_url) = parse_html(reult_string) {
            let mut new_result = reqwest::get(feed_url.get()).context(FeedParserErrorKind::Http)?;
            let mut new_result_string = String::new();
            let _ = new_result
                .read_to_string(&mut new_result_string)
                .context(FeedParserErrorKind::Http)?;
            return parse_feed(new_result_string.as_bytes(), url, id, title, order_id);
        }
    }

    parse_feed(&result_buffer, url, id, title, order_id)
}

fn parse_html(html: &str) -> Result<Url, FeedParserError> {
    let parser = Parser::default_html();
    let xpath = "//link[@rel='alternate']";
    if let Ok(doc) = parser.parse_string(html) {
        if let Ok(xpath_ctx) = Context::new(&doc) {
            if let Ok(result) = xpath_ctx.evaluate(xpath) {
                let result = result.get_nodes_as_vec();
                if let Some(node) = result.get(0) {
                    if let Some(url) = node.get_property("href") {
                        debug!("Parsing Html yielded feed: {}", url);
                        let url = Url::parse(&url)
                            .map_err(|err| {
                                warn!("Failed to parse url: {}", url);
                                err
                            })
                            .context(FeedParserErrorKind::Url)?;
                        return Ok(url);
                    } else {
                        warn!("<link> tag is missin href property");
                    }
                } else {
                    warn!("xpath didn't yield any results: {}", xpath);
                }
            } else {
                warn!("xpath evaluation failed: {}", xpath);
            }
        }
    } else {
        warn!("Failed to parse HTML");
    }

    Err(FeedParserErrorKind::Html)?
}

fn check_content_type(header: Option<&reqwest::header::ContentType>) -> Option<ContentType> {
    if let Some(header) = header {
        let type_ = header.type_();
        let subtype = header.subtype();
        let suffix = header.suffix();

        if type_ == "text" && subtype == "html" && suffix == None {
            return Some(ContentType::Html);
        }

        if type_ == "application" && subtype == "atom" {
            return Some(ContentType::Atom);
        }

        if (type_ == "text" && subtype == "xml") || (type_ == "application" && subtype == "rss") {
            return Some(ContentType::Rss);
        }

        if type_ == "application" && subtype == "vnd.api" {
            if let Some(suffix) = suffix {
                if suffix == "json" {
                    return Some(ContentType::JsonFeed);
                }
            }
        }
    }

    None
}

fn parse_feed(
    feed: &[u8],
    url: &Url,
    id: &FeedID,
    title: Option<&str>,
    order_id: Option<i32>,
) -> Result<Feed, FeedParserError> {
    // check if feed is atom feed
    if let Ok(feed) = parse_atom_feed(BufReader::new(feed), url, id, title, order_id) {
        return Ok(feed);
    }

    // check if feed is rss feed
    if let Ok(feed) = parse_rss_feed(BufReader::new(feed), url, id, title, order_id) {
        return Ok(feed);
    }

    if let Ok(feed) = parse_json_feed(BufReader::new(feed), url, id, title, order_id) {
        return Ok(feed);
    }

    warn!("Neither atom nor rss parsing succeeded: {}", url);
    Err(FeedParserErrorKind::Rss)?
}

fn parse_json_feed<R: Read>(
    feed: BufReader<R>,
    url: &Url,
    id: &FeedID,
    title: Option<&str>,
    order_id: Option<i32>,
) -> Result<Feed, FeedParserError> {
    if let Ok(json_feed) = jsonfeed::from_reader(feed) {
        let title = match title {
            Some(title) => title.to_string(),
            None => json_feed.title,
        };
        let mut website: Option<Url> = None;
        if let Some(url_str) = json_feed.home_page_url {
            if let Ok(url) = Url::parse(&url_str) {
                website = Some(url);
            }
        }
        let mut icon_url: Option<Url> = None;
        if let Some(icon_str) = json_feed.icon {
            if let Ok(url) = Url::parse(&icon_str) {
                icon_url = Some(url);
            }
        }
        let feed = Feed {
            feed_id: id.clone(),
            label: title,
            website: website,
            feed_url: Some(url.clone()),
            icon_url: icon_url,
            sort_index: order_id,
        };

        return Ok(feed);
    }

    Err(FeedParserErrorKind::JsonFeed)?
}

fn parse_atom_feed<R: Read>(
    feed: BufReader<R>,
    url: &Url,
    id: &FeedID,
    title: Option<&str>,
    order_id: Option<i32>,
) -> Result<Feed, FeedParserError> {
    if let Ok(atom_feed) = AtomFeed::read_from(feed) {
        let title = match title {
            Some(title) => title,
            None => atom_feed.title(),
        };
        let mut website: Option<Url> = None;
        if atom_feed.links().len() > 0 {
            // see if there is a link with rel='alternate' -> this is probably what we want
            if let Some(link) = atom_feed
                .links()
                .into_iter()
                .find(|link| link.rel() == "alternate")
            {
                if let Ok(url) = Url::parse(link.href()) {
                    website = Some(url);
                }
            }
            // otherwise just take the first link
            else if let Ok(url) = Url::parse(atom_feed.links()[0].href()) {
                website = Some(url);
            }
        }
        let icon_url = match atom_feed.icon() {
            Some(url_str) => match Url::parse(url_str) {
                Ok(url) => Some(url),
                Err(_) => None,
            },
            None => None,
        };

        let feed = Feed {
            feed_id: id.clone(),
            label: title.to_string(),
            website: website,
            feed_url: Some(url.clone()),
            icon_url: icon_url,
            sort_index: order_id,
        };

        return Ok(feed);
    }

    Err(FeedParserErrorKind::Atom)?
}

fn parse_rss_feed<R: Read>(
    feed: BufReader<R>,
    url: &Url,
    id: &FeedID,
    title: Option<&str>,
    order_id: Option<i32>,
) -> Result<Feed, FeedParserError> {
    if let Ok(rss_feed) = RssFeed::read_from(feed) {
        let title = match title {
            Some(title) => title,
            None => rss_feed.title(),
        };
        let website = match Url::parse(rss_feed.link()) {
            Ok(website) => Some(website),
            Err(_) => None,
        };
        let icon_url = match rss_feed.image() {
            Some(image) => match Url::parse(image.url()) {
                Ok(url) => Some(url),
                Err(_) => None,
            },
            None => None,
        };

        let feed = Feed {
            feed_id: id.clone(),
            label: title.to_string(),
            website: website,
            feed_url: Some(url.clone()),
            icon_url: icon_url,
            sort_index: order_id,
        };

        return Ok(feed);
    }
    Err(FeedParserErrorKind::Rss)?
}

#[cfg(test)]
mod tests {
    use models::{FeedID, Url};
    use util::feed_parser;

    #[test]
    pub fn golem_atom() {
        let url_text = "https://rss.golem.de/rss.php?feed=ATOM1.0";
        let url = Url::parse(url_text).unwrap();
        let feed_id = FeedID::new(url_text);
        let feed = feed_parser::download_and_parse_feed(&url, &feed_id, None, None).unwrap();

        assert_eq!(feed.label, "Golem.de");
        assert_eq!(
            feed.icon_url.unwrap().to_string(),
            "https://www.golem.de/favicon.ico"
        );
        assert_eq!(feed.website.unwrap().to_string(), "https://www.golem.de/");
    }

    #[test]
    pub fn planet_gnome_rss() {
        let url_text = "http://planet.gnome.org/rss20.xml";
        let url = Url::parse(url_text).unwrap();
        let feed_id = FeedID::new(url_text);
        let feed = feed_parser::download_and_parse_feed(&url, &feed_id, None, None).unwrap();

        assert_eq!(feed.label, "Planet GNOME");
        assert_eq!(feed.icon_url, None);
        assert_eq!(
            feed.website.unwrap().to_string(),
            "http://planet.gnome.org/"
        );
    }
}
