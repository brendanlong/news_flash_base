mod error;

use self::error::{OpmlError, OpmlErrorKind};
use failure::ResultExt;
use models::{Category, CategoryID, Feed, FeedID, FeedMapping, Url, NEWSFLASH_TOPLEVEL};
use std::str;
use sxd_document::{dom, parser, writer, Package};
use util::feed_parser;

pub fn generate_opml(
    categories: &Vec<Category>,
    feeds: &Vec<Feed>,
    mappings: &Vec<FeedMapping>,
) -> Result<String, OpmlError> {
    let package = Package::new();
    let doc = package.as_document();

    let opml_node = doc.create_element("opml");
    opml_node.set_attribute_value("version", "1.0");
    doc.root().append_child(opml_node);

    let toplevel_id = NEWSFLASH_TOPLEVEL.clone();
    write_categories(categories, feeds, mappings, &toplevel_id, &opml_node, &doc);

    let mut output: Vec<u8> = Vec::new();
    writer::format_document(&doc, &mut output).context(OpmlErrorKind::Xml)?;
    let output = str::from_utf8(&output).context(OpmlErrorKind::Utf8)?;

    Ok(output.to_owned())
}

pub fn write_categories(
    categories: &Vec<Category>,
    feeds: &Vec<Feed>,
    mappings: &Vec<FeedMapping>,
    parent_id: &CategoryID,
    parent_node: &dom::Element,
    doc: &dom::Document,
) {
    let filtered_categories: Vec<&Category> = categories
        .into_iter()
        .filter(|category| &category.parent == parent_id)
        .collect();

    for category in filtered_categories {
        let category_node = doc.create_element("outline");
        category_node.set_attribute_value("title", &category.label);
        category_node.set_attribute_value("text", &category.label);
        parent_node.append_child(category_node);

        write_categories(
            categories,
            feeds,
            mappings,
            &category.category_id,
            &category_node,
            doc,
        );
    }

    let feed_ids: Vec<&FeedID> = mappings
        .into_iter()
        .filter(|mapping| &mapping.category_id == parent_id)
        .map(|mapping| &mapping.feed_id)
        .collect();

    let feeds: Vec<&Feed> = feeds
        .into_iter()
        .filter(|feed| feed_ids.contains(&&feed.feed_id))
        .collect();

    for feed in feeds {
        if let Some(ref xml_url) = &feed.feed_url {
            let feed_node = doc.create_element("outline");
            feed_node.set_attribute_value("title", &feed.label);
            feed_node.set_attribute_value("text", &feed.label);
            feed_node.set_attribute_value("type", "rss");
            feed_node.set_attribute_value("xmlUrl", &format!("{}", xml_url));
            if let Some(ref website) = &feed.website {
                feed_node.set_attribute_value("htmlUrl", &format!("{}", website));
            }
            parent_node.append_child(feed_node);
        }
    }
}

pub fn parse_opml(
    opml_string: &str,
    parse_feeds: bool,
) -> Result<(Vec<Category>, Vec<Feed>, Vec<FeedMapping>), OpmlError> {
    let package = parser::parse(opml_string).map_err(|(pos, errors)| {
        error!("Failed to parse opml file to xml document");
        error!("Error at pos {}: {:?}", pos, errors);
        OpmlErrorKind::Xml
    })?;
    let document = package.as_document();
    let root = document.root();

    let opml_tag = root.children();
    let mut category_vec: Vec<Category> = Vec::new();
    let mut feed_vec: Vec<Feed> = Vec::new();
    let mut mapping_vec: Vec<FeedMapping> = Vec::new();

    for element in opml_tag {
        if let Some(head_and_body) = parse_opml_tag(element) {
            for element in head_and_body {
                if let Some(outlines) = parse_body(element) {
                    let mut category_sort_index: i32 = 0;
                    let mut feed_sort_index: i32 = 0;
                    for outline in outlines {
                        parse_outline(
                            outline,
                            parse_feeds,
                            &NEWSFLASH_TOPLEVEL,
                            &mut category_sort_index,
                            &mut feed_sort_index,
                            &mut category_vec,
                            &mut feed_vec,
                            &mut mapping_vec,
                        );
                    }
                    return Ok((category_vec, feed_vec, mapping_vec));
                }
                // only one body outline allowed, so return after parsing the first one
                // ignoring any further outline children
            }
            warn!("Opml: No outline tag inside of body found");
            return Err(OpmlErrorKind::Empty)?;
        }
    }

    Err(OpmlErrorKind::Body)?
}

fn parse_outline(
    outline: dom::ChildOfElement,
    parse_feeds: bool,
    category_id: &CategoryID,
    category_sort_index: &mut i32,
    feed_sort_index: &mut i32,
    category_vec: &mut Vec<Category>,
    feed_vec: &mut Vec<Feed>,
    mapping_vec: &mut Vec<FeedMapping>,
) {
    // could either be a category or a feed
    if let Some((category, outlines)) = parse_category(outline, category_sort_index) {
        let category_id = category.category_id.clone();
        category_vec.push(category);
        for outline in outlines {
            parse_outline(
                outline,
                parse_feeds,
                &category_id,
                category_sort_index,
                feed_sort_index,
                category_vec,
                feed_vec,
                mapping_vec,
            );
        }
    } else if let Some((feed, mapping)) =
        parse_feed(outline, category_id, feed_sort_index, parse_feeds)
    {
        feed_vec.push(feed);
        mapping_vec.push(mapping);
    }
}

fn parse_opml_tag(opml_tag: dom::ChildOfRoot) -> Option<Vec<dom::ChildOfElement>> {
    let opml_tag = opml_tag.element()?;
    if "opml" == opml_tag.name().local_part() {
        return Some(opml_tag.children());
    }
    None
}

fn parse_body(body: dom::ChildOfElement) -> Option<Vec<dom::ChildOfElement>> {
    let body_tag = body.element()?;
    if "body" == body_tag.name().local_part() {
        return Some(body_tag.children());
    }
    None
}

fn parse_category<'a>(
    outline: dom::ChildOfElement<'a>,
    sort_index: &mut i32,
) -> Option<(Category, Vec<dom::ChildOfElement<'a>>)> {
    let category_outline = outline.element()?;
    if "outline" == category_outline.name().local_part()
        && None == category_outline.attribute_value("xmlUrl")
    {
        let title = match category_outline.attribute_value("title") {
            Some(title) => title,
            None => category_outline.attribute_value("text")?,
        };
        let category = Category {
            category_id: CategoryID::new(title),
            label: title.to_owned(),
            sort_index: Some(*sort_index),
            parent: NEWSFLASH_TOPLEVEL.clone(),
        };
        *sort_index += 1;
        return Some((category, category_outline.children()));
    }
    None
}

fn parse_feed(
    outline: dom::ChildOfElement,
    category_id: &CategoryID,
    sort_index: &mut i32,
    parse_feeds: bool,
) -> Option<(Feed, FeedMapping)> {
    let feed_outline = outline.element()?;
    if "outline" == feed_outline.name().local_part() {
        if let Some("rss") = feed_outline.attribute_value("type") {
            // prefer optional "title" attribute, fall back to mandatory "text" attribute
            let title = match feed_outline.attribute_value("title") {
                Some(title) => title,
                None => feed_outline.attribute_value("text")?,
            };
            let xml_url = feed_outline.attribute_value("xmlUrl")?;
            let feed_id = FeedID::new(xml_url);
            let xml_url = Url::parse(xml_url).ok()?;
            let mapping = FeedMapping {
                feed_id: feed_id.clone(),
                category_id: category_id.clone(),
            };
            let website = match feed_outline.attribute_value("htmlUrl") {
                Some(html_url) => {
                    let html_url = Url::parse(html_url).ok()?;
                    Some(html_url)
                }
                None => None,
            };

            let mut feed = Feed {
                feed_id: feed_id.clone(),
                label: title.to_owned(),
                website: website,
                feed_url: Some(xml_url.clone()),
                icon_url: None,
                sort_index: Some(*sort_index),
            };

            if parse_feeds {
                if let Ok(parsed_feed) = feed_parser::download_and_parse_feed(
                    &xml_url,
                    &feed_id,
                    Some(title),
                    Some(*sort_index),
                ) {
                    feed = parsed_feed;
                } else {
                    warn!(
                        "Parsing of feed '{}' failed, falling back to data from opml",
                        xml_url
                    )
                }
            }

            *sort_index += 1;
            return Some((feed, mapping));
        }
    }
    None
}

#[cfg(test)]
mod tests {
    use models::{Category, CategoryID, Feed, FeedID, FeedMapping, Url, NEWSFLASH_TOPLEVEL};
    use util::opml;

    #[test]
    pub fn opml_export_1() {
        let mut categories: Vec<Category> = Vec::new();
        let mut feeds: Vec<Feed> = Vec::new();
        let mut mappings: Vec<FeedMapping> = Vec::new();

        categories.push(Category {
            category_id: CategoryID::new("cat1"),
            label: "Category 1".to_owned(),
            sort_index: Some(0),
            parent: NEWSFLASH_TOPLEVEL.clone(),
        });

        categories.push(Category {
            category_id: CategoryID::new("cat2"),
            label: "Category 2".to_owned(),
            sort_index: Some(1),
            parent: NEWSFLASH_TOPLEVEL.clone(),
        });

        categories.push(Category {
            category_id: CategoryID::new("cat3"),
            label: "Category 3".to_owned(),
            sort_index: Some(0),
            parent: CategoryID::new("cat1"),
        });

        feeds.push(Feed {
            feed_id: FeedID::new("feed1"),
            label: "Feed 1".to_owned(),
            website: Some(Url::parse("http://golem.de").unwrap()),
            feed_url: Some(Url::parse("https://rss.golem.de/rss.php?feed=RSS2.0").unwrap()),
            icon_url: None,
            sort_index: Some(0),
        });

        feeds.push(Feed {
            feed_id: FeedID::new("feed2"),
            label: "Feed 2".to_owned(),
            website: Some(Url::parse("http://golem.de").unwrap()),
            feed_url: Some(Url::parse("https://rss.golem.de/rss.php?feed=ATOM1.0").unwrap()),
            icon_url: None,
            sort_index: Some(1),
        });

        feeds.push(Feed {
            feed_id: FeedID::new("feed3"),
            label: "Feed 3".to_owned(),
            website: Some(Url::parse("http://heise.de").unwrap()),
            feed_url: Some(Url::parse("https://www.heise.de/newsticker/heise-atom.xml").unwrap()),
            icon_url: None,
            sort_index: Some(2),
        });

        mappings.push(FeedMapping {
            feed_id: FeedID::new("feed1"),
            category_id: NEWSFLASH_TOPLEVEL.clone(),
        });

        mappings.push(FeedMapping {
            feed_id: FeedID::new("feed1"),
            category_id: CategoryID::new("cat1"),
        });

        mappings.push(FeedMapping {
            feed_id: FeedID::new("feed2"),
            category_id: CategoryID::new("cat2"),
        });

        mappings.push(FeedMapping {
            feed_id: FeedID::new("feed3"),
            category_id: CategoryID::new("cat3"),
        });

        let opml_string = opml::generate_opml(&categories, &feeds, &mappings).unwrap();
        let opml_reference = r#"<?xml version='1.0'?><opml version='1.0'><outline title='Category 1' text='Category 1'><outline title='Category 3' text='Category 3'><outline title='Feed 3' text='Feed 3' type='rss' xmlUrl='https://www.heise.de/newsticker/heise-atom.xml' htmlUrl='http://heise.de/'/></outline><outline title='Feed 1' text='Feed 1' type='rss' xmlUrl='https://rss.golem.de/rss.php?feed=RSS2.0' htmlUrl='http://golem.de/'/></outline><outline title='Category 2' text='Category 2'><outline title='Feed 2' text='Feed 2' type='rss' xmlUrl='https://rss.golem.de/rss.php?feed=ATOM1.0' htmlUrl='http://golem.de/'/></outline><outline title='Feed 1' text='Feed 1' type='rss' xmlUrl='https://rss.golem.de/rss.php?feed=RSS2.0' htmlUrl='http://golem.de/'/></opml>"#;

        assert_eq!(&opml_string, opml_reference);
    }

    #[test]
    pub fn opml_import_1() {
        let opml_string = r#"<?xml version="1.0" encoding="UTF-8"?>
        <opml version="1.0">
            <head>
                <title>Sample OPML file for RSSReader</title>
            </head>
            <body>
                <outline title="News" text="News">
                    <outline text="Big News Finland" title="Big News Finland" type="rss" xmlUrl="http://www.bignewsnetwork.com/?rss=37e8860164ce009a"/>
                    <outline text="Euronews" title="Euronews" type="rss" xmlUrl="http://feeds.feedburner.com/euronews/en/news/"/>
                    <outline text="Reuters Top News" title="Reuters Top News" type="rss" xmlUrl="http://feeds.reuters.com/reuters/topNews"/>
                    <outline text="Yahoo Europe" title="Yahoo Europe" type="rss" xmlUrl="http://rss.news.yahoo.com/rss/europe"/>
                </outline>

                <outline title="Leisure" text="Leisure">
                    <outline text="CNN Entertainment" title="CNN Entertainment" type="rss" xmlUrl="http://rss.cnn.com/rss/edition_entertainment.rss"/>
                    <outline text="E! News" title="E! News" type="rss" xmlUrl="http://uk.eonline.com/syndication/feeds/rssfeeds/topstories.xml"/>
                    <outline text="Hollywood Reporter" title="Hollywood Reporter" type="rss" xmlUrl="http://feeds.feedburner.com/thr/news"/>
                    <outline text="Reuters Entertainment" title="Reuters Entertainment" type="rss"  xmlUrl="http://feeds.reuters.com/reuters/entertainment"/>
                    <outline text="Reuters Music News" title="Reuters Music News" type="rss" xmlUrl="http://feeds.reuters.com/reuters/musicNews"/>
                    <outline text="Yahoo Entertainment" title="Yahoo Entertainment" type="rss" xmlUrl="http://rss.news.yahoo.com/rss/entertainment"/>
                </outline>

                <outline title="Sports" text="Sports">
                    <outline text="Formula 1" title="Formula 1" type="rss" xmlUrl="http://www.formula1.com/rss/news/latest.rss"/>
                    <outline text="MotoGP" title="MotoGP" type="rss" xmlUrl="http://rss.crash.net/crash_motogp.xml"/>
                    <outline text="N.Y.Times Track And Field" title="N.Y.Times Track And Field" type="rss" xmlUrl="http://topics.nytimes.com/topics/reference/timestopics/subjects/t/track_and_field/index.html?rss=1"/>
                    <outline text="Reuters Sports" title="Reuters Sports" type="rss" xmlUrl="http://feeds.reuters.com/reuters/sportsNews"/>
                    <outline text="Yahoo Sports NHL" title="Yahoo Sports NHL" type="rss" xmlUrl="http://sports.yahoo.com/nhl/rss.xml"/>
                    <outline text="Yahoo Sports" title="Yahoo Sports" type="rss" xmlUrl="http://rss.news.yahoo.com/rss/sports"/>
                </outline>

                <outline title="Tech" text="Tech">
                    <outline text="Coding Horror" title="Coding Horror" type="rss" xmlUrl="http://feeds.feedburner.com/codinghorror/"/>
                    <outline text="Gadget Lab" title="Gadget Lab" type="rss" xmlUrl="http://www.wired.com/gadgetlab/feed/"/>
                    <outline text="Gizmodo" title="Gizmodo" type="rss" xmlUrl="http://gizmodo.com/index.xml"/>
                    <outline text="Reuters Technology" title="Reuters Technology" type="rss" xmlUrl="http://feeds.reuters.com/reuters/technologyNews"/>
                </outline>
            </body>
        </opml>
        "#;

        let (categories, feeds, mappings) = opml::parse_opml(opml_string, false).unwrap();

        assert_eq!(categories.len(), 4);
        assert_eq!(feeds.len(), 20);
        assert_eq!(mappings.len(), feeds.len());
        assert_eq!(categories.get(2).unwrap().label, "Sports");
        assert_eq!(feeds.get(6).unwrap().label, "Hollywood Reporter");
    }

    #[test]
    pub fn opml_import_2() {
        let opml_string = r#"<?xml version="1.0" encoding="utf-8"?>
        <opml version="1.1">
            <head>
                <title>Blogtrottr OPML export</title>
                <dateCreated>Tue, 14 Apr 2015 20:43:49 +0000</dateCreated>
            </head>
            <body>
                <outline text="Subscriptions">
                    <outline title="Create your own games» Game Creation Blog by Koonsolo" text="A blog on how to create your own computer games (by Koen Witters)" type="rss" xmlUrl="http://dev.koonsolo.com/feed/"/>
                    <outline title="Digitanks" text="" type="rss" xmlUrl="http://digitanks.com/feed/"/>
                    <outline title="Jacques Mattheij" text="" type="rss" xmlUrl="http://jacquesmattheij.com/rss.xml"/>
                    <outline title="jwz" text="jwz - LiveJournal.com" type="rss" xmlUrl="http://jwz.livejournal.com/data/rss"/>
                    <outline title="Pyevolve" text="by Christian S. Perone" type="rss" xmlUrl="http://feeds2.feedburner.com/pyevolve"/>
                    <outline title="Yipit Django Blog" text="Django Tips and Best Practices" type="rss" xmlUrl="http://feeds.feedburner.com/YipitDjangoBlog?format=xml"/>
                    <outline title="802-BIKEGUY" text="Bike pedals before gas pedals!" type="rss" xmlUrl="http://www.802bikeguy.com/feed/"/>
                    <outline title="A Smart Bear" type="rss" xmlUrl="http://feeds.feedburner.com/blogspot/smartbear"/>
                    <outline title="AVC" text="Musings of a VC in NYC" type="rss" xmlUrl="http://feeds.feedburner.com/avc"/>
                    <outline title="ActiveBlog:  Insights on Code, the Cloud and More" text="" type="rss" xmlUrl="http://blogs.activestate.com/feed"/>
                    <outline title="Algorithm.co.il" text="Algorithms, for the heck of it" type="rss" xmlUrl="http://www.algorithm.co.il/blogs/index.php/feed/"/>
                    <outline title="Art Of Community Online" text="The Book On Community Management, by Jono Bacon" type="rss" xmlUrl="http://www.artofcommunityonline.org/feed/"/>
                    <outline title="BetaList" text="BetaList provides an overview of upcoming internet startups. Discover and get early access to the future." type="rss" xmlUrl="http://betali.st/rss"/>
                    <outline title="Black&amp;White™" text="Essay" type="rss" xmlUrl="http://000fff.org/feed/"/>
                    <outline title="Critical-Gaming Blog" text="" type="rss" xmlUrl="http://critical-gaming.squarespace.com/blog/rss.xml"/>
                    <outline title="Blog - Stack Exchange" text="free, community powered Q&amp;A" type="rss" xmlUrl="http://blog.stackoverflow.com/feed/"/>
                    <outline title="Brainy Gamer" text="Thoughtful conversation about video games" type="rss" xmlUrl="http://feeds.feedburner.com/brainygamer"/>
                    <outline title="Brandon Staggs .Com" text="Software, Society, the Bible, Politics, and everything else." type="rss" xmlUrl="http://www.brandonstaggs.com/feed/atom/"/>
                    <outline title="Bruce Eckel's Weblog" text="Artima Weblogs is a community of bloggers" type="rss" xmlUrl="http://www.artima.com/weblogs/feeds/bloggers/beckel.rss"/>
                    <outline title="Chocolate Hammer" text="Home of Vatsy and Bruno, Let's Plays, Several Ancient Ghosts" type="rss" xmlUrl="http://www.chocolatehammer.org/?feed=rss2"/>
                    <outline title="Coding Horror" text="programming and human factors" type="rss" xmlUrl="http://feeds.feedburner.com/codinghorror"/>
                    <outline title="Coding the Wheel" text="Building the collective hamster wheel, one line of code at a time" type="rss" xmlUrl="http://feeds.feedburner.com/codingthewheel"/>
                    <outline title="Zukifying Security" text="" type="rss" xmlUrl="http://imthezuk.blogspot.com/feeds/posts/default?alt=rss"/>
                    <outline title="Zukifying Security" text="" type="rss" xmlUrl="http://imthezuk.blogspot.com/feeds/posts/default"/>
                    <outline title="Daniel Lew's Coding Thoughts" text="I post random things" type="rss" xmlUrl="http://daniel-codes.blogspot.com/feeds/posts/default?alt=rss"/>
                    <outline title="Dead Panic" text="" type="rss" xmlUrl="http://deadpanic.com/rss.xml"/>
                    <outline title="Derren Brown » Blog" text="The Official Site" type="rss" xmlUrl="http://derrenbrownart.com/blog/feed/"/>
                    <outline title="Dilbert Daily Strip" text="" type="rss" xmlUrl="http://feeds.dilbert.com/DilbertDailyStrip"/>
                    <outline title="Scott Adams Blog" text="" type="rss" xmlUrl="http://feed.dilbert.com/dilbert/blog"/>
                    <outline title="Fog Creek Blog" text="Helping the world's best developers make better software" type="rss" xmlUrl="http://www.fogcreek.com/FogBugz/blog/syndication.axd"/>
                    <outline title="Gamasutra News" text="Gamasutra News" type="rss" xmlUrl="http://feeds.feedburner.com/GamasutraFeatureArticles/"/>
                    <outline title="Gigantt Blog" text="" type="rss" xmlUrl="http://feeds.feedburner.com/GiganttBlog"/>
                    <outline title="Google Engineering Tools" text="" type="rss" xmlUrl="http://google-engtools.blogspot.com/atom.xml"/>
                    <outline title="Guido van Rossum's Weblog" text="Artima Weblogs is a community" type="rss" xmlUrl="http://www.artima.com/weblogs/feeds/bloggers/guido.rss"/>
                    <outline title="Henrik's Blog" text="" type="rss" xmlUrl="http://hforsten.com/feeds/all.atom.xml"/>
                    <outline title="Heroes Happen Here {Comic Series}" text="Brought to you by Microsoft and Seagate" type="rss" xmlUrl="http://blogs.technet.com/hhh_comic/rss.xml"/>
                    <outline title="HTML5 Security Cheatsheet" text="Page2RSS " type="rss" xmlUrl="http://page2rss.com/rss/957dc181afad20429e4633d20b1ca5a2"/>
                    <outline title="‫iAndroid - האנדרואיד של גוגל - בעברית !‬" text="‫asdf‬" type="rss" xmlUrl="http://iandroid.co.il/dr-iandroid/feed"/>
                    <outline title="Israel Venture Capital 2.0" text="General Partner" type="rss" xmlUrl="http://feeds.feedburner.com/typepad/BJMB"/>
                    <outline title="James Breckenridge" text="Online, Marketing &amp; General Rubbish" type="rss" xmlUrl="http://www.jamesbreckenridge.co.uk/feed"/>
                    <outline title="Joel on Software" text="Painless Software Management" type="rss" xmlUrl="http://www.joelonsoftware.com/rss.xml"/>
                    <outline title="Man Vs. Debt" text="Sell your crap" type="rss" xmlUrl="http://feeds2.feedburner.com/manvsdebt"/>
                    <outline title="My Boring Ass Life" text="Kevin Smith's Boring Ass Life" type="rss" xmlUrl="http://silentbobspeaks.com/?feed=rss2"/>
                    <outline title="Ned Batchelder's blog" text="Ned Batchelder's personal blog." type="rss" xmlUrl="http://nedbatchelder.com/blog/atom.xml"/>
                    <outline title="Nelson's Weblog" text="Occasional blog, powered by Blosxom" type="rss" xmlUrl="http://www.somebits.com/weblog/index.atom"/>
                    <outline title="Neopythonic" text="Ramblings through technology" type="rss" xmlUrl="http://neopythonic.blogspot.com/feeds/posts/default"/>
                    <outline title="New Rules" text="This is a blog" type="rss" xmlUrl="http://feedproxy.google.com/NewRules"/>
                    <outline title="Paul Graham: Essays" text="Scraped feed provided by aaronsw.com" type="rss" xmlUrl="http://www.aaronsw.com/2002/feeds/pgessays.rss"/>
                    <outline title="Penny Arcade" text="News Fucker 5000" type="rss" xmlUrl="http://feeds.penny-arcade.com/pa-mainsite"/>
                    <outline title="Philosophy Bro" text="Philosophy is hard" type="rss" xmlUrl="http://feeds.feedburner.com/PhilosophyBro"/>
                    <outline title="Procrastineering" text="Project blog for Johnny Chung Lee" type="rss" xmlUrl="http://procrastineering.blogspot.com/feeds/posts/default"/>
                    <outline title="Robert's talk" text="Web development and Internet trends" type="rss" xmlUrl="http://feeds.feedburner.com/robertnyman"/>
                    <outline title="SaaS Adventures" text="Tom's attempts to make money" type="rss" xmlUrl="http://feeds.feedburner.com/SaasAdventures"/>
                    <outline title="secretGeek" text="secretGeek" type="rss" xmlUrl="http://www.secretgeek.net/rss.asp"/>
                    <outline title="Seth Godin's Blog" text="Seth Godin's riffs on marketing" type="rss" xmlUrl="http://feeds.feedburner.com/typepad/sethsmainblog"/>
                    <outline title="Shifty Jelly's blog of mystery" text="because some things were meant to be shifty..." type="rss" xmlUrl="https://shiftyjelly.wordpress.com/feed/"/>
                    <outline title="Stephen Fry's PODGRAMS" text="Stephen Fry, British actor" type="rss" xmlUrl="http://www.stephenfry.com/media/audio/rss/m4a/"/>
                    <outline title="Stevey's Blog Rants" text="Random whining and stuff." type="rss" xmlUrl="http://steve-yegge.blogspot.com/feeds/posts/default"/>
                    <outline title="Successful Software" text="...requires more than just good programming." type="rss" xmlUrl="http://successfulsoftware.net/feed/"/>
                    <outline title="Surf Roots, Software Thoughts" text="" type="rss" xmlUrl="http://alexlod.com/feed/"/>
                    <outline title="Sveder's Blog" text="Sveder's blog about programming and stuff" type="rss" xmlUrl="http://feeds.feedburner.com/Sveder"/>
                    <outline title="System Malfunction" text="An epic tale of courage" type="rss" xmlUrl="http://www.systemmalfunction.com/feeds/posts/default?alt=rss"/>
                    <outline title="The Dilbert Blog" text="Dilbert humor" type="rss" xmlUrl="http://dilbertblog.typepad.com/the_dilbert_blog/rss.xml"/>
                    <outline title="The Escapist" text="Shamus Young flings" type="rss" xmlUrl="http://www.escapistmagazine.com/rss/articles/comics/stolen-pixels"/>
                    <outline title="The Ludologist" text="My name is Jesper Juul" type="rss" xmlUrl="http://www.jesperjuul.net/ludologist/feed"/>
                    <outline title="The Radioactive Yak" text="" type="rss" xmlUrl="http://feeds.feedburner.com/TheRadioactiveYak"/>
                    <outline title="The Resigned Gamer" text="Everything I hate about the thing I love the most." type="rss" xmlUrl="http://resignedgamer.blogspot.com/feeds/posts/default"/>
                    <outline title="The Web World" text="Front-end and back-end web development" type="rss" xmlUrl="http://ronreiterdotcom.wordpress.com/feed/"/>
                    <outline title="Twenty Sided" text="A Website for your Internet" type="rss" xmlUrl="http://www.shamusyoung.com/twentysidedtale/?feed=rss2"/>
                    <outline title="Venture Hacks" text="Good advice for startups" type="rss" xmlUrl="http://feeds.venturehacks.com/venturehacks"/>
                    <outline title="Каждая сова днем выглядит глупо" text="Каждая сова днем выглядит глупо - LiveJournal.com" type="rss" xmlUrl="http://the-owls1.livejournal.com/data/rss"/>
                    <outline title="‫אנדרואיד באז שיווק ופרסום‬" text="‫Just another iAndroid - הפורטל הישראלי לפלטפורמת אנדרואיד‬" type="rss" xmlUrl="http://iandroid.co.il/android-marketing/feed"/>
                    <outline title="ן להתעדכן גם כן" type="rss" xmlUrl="http://feeds.feedburner.com/ipod-hebrew"/>
                    <outline title="רוכבים את ישראל" text="" type="rss" xmlUrl="http://ridingisrael.blogspot.com/feeds/posts/default"/>
                    <outline title="Papa HuHu" text="Papa HuHu - LiveJournal.com" type="rss" xmlUrl="http://papahuhu.livejournal.com/data/rss"/>
                    <outline title="A Gamer!" text="Gamer" type="rss" xmlUrl="http://marmamic.tumblr.com/rss"/>
                    <outline title="The Django weblog" text="Latest news about Django" type="rss" xmlUrl="http://www.djangoproject.com/rss/weblog/"/>
                    <outline title="Matt Hartzell's China Blog" text="" type="rss" xmlUrl="http://matthartzell.blogspot.com/feeds/posts/default?alt=rss"/>
                    <outline title="Significant Bits" text="On videogame design and such." type="rss" xmlUrl="http://feeds.feedburner.com/SignificantBits"/>
                    <outline title="Taiwan In Cycles" text="" type="rss" xmlUrl="http://taiwanincycles.blogspot.com/feeds/posts/default?alt=rss"/>
                    <outline title="Synapticism" text="An experiential journal of synchronicity and connection." type="rss" xmlUrl="http://synapticism.com/feed/"/>
                    <outline title="Feld Thoughts" text="" type="rss" xmlUrl="http://feeds.feedburner.com/FeldThoughts"/>
                    <outline title="gil ben-artzy" text="Thoughts, observations and random musings..." type="rss" xmlUrl="http://gilbenartzy.com/feed/"/>
                    <outline title="xkcd.com" text="" type="rss" xmlUrl="http://xkcd.com/atom.xml"/>
                    <outline title="Any.DO Tech Blog" text="In search for a better way to make things happen" type="rss" xmlUrl="http://tech.any.do/feed/"/>
                    <outline title="The Stack" text="A completist plays old games" type="rss" xmlUrl="http://www.wurb.com/stack/feed"/>
                    <outline title="מה יש לאכול" text="החיים, מתכונים ומה שבינהם" type="rss" xmlUrl="http://www.tals-cooking.com/?feed=rss2"/>
                    <outline title="Significant Bits" text="On videogame design and such." type="rss" xmlUrl="http://feeds.feedburner.com/SignificantBits"/>
                    <outline title="Eric Sink" text="SourceGear Founder" type="rss" xmlUrl="http://www.ericsink.com/rss.xml"/>
                    <outline title="HackMii" text="Notes from inside your Wii" type="rss" xmlUrl="http://hackmii.com/feed/"/>
                    <outline title="User Agent Man" text="A Blog about Client Side Web Technology" type="rss" xmlUrl="http://www.useragentman.com/blog/feed/"/>
                    <outline title="Valve" text="Valve Software Blogs" type="rss" xmlUrl="http://blogs.valvesoftware.com/feed/"/>
                    <outline title="0 and 1" text="The Ugly Website" type="rss" xmlUrl="http://oandone.blogspot.com/feeds/posts/default?alt=rss"/>
                    <outline title="M86 Security Labs Blog" text="M86 Security Labs blog for all news related to security." type="rss" xmlUrl="http://labs.m86security.com/feed/"/>
                    <outline title="Mozilla Web Development" text="For make benefit of glorious tubes" type="rss" xmlUrl="http://blog.mozilla.org/webdev/feed/"/>
                    <outline title="Sea of Memes" text="Let's Code... an MMO!" type="rss" xmlUrl="http://www.sea-of-memes.com/rss.xml"/>
                    <outline title="The Word of Notch" text="Rarely updated rambling, ranting, and informing." type="rss" xmlUrl="http://notch.tumblr.com/rss"/>
                    <outline title="Toolness" text="The Blog of Atul Varma" type="rss" xmlUrl="http://www.toolness.com/wp/?feed=rss2"/>
                    <outline title="Toolness" text="The Blog of Atul Varma" type="rss" xmlUrl="http://www.toolness.com/wp/feed/"/>
                    <outline title="Tynan" text="My name is Tynan" type="rss" xmlUrl="http://feeds.feedburner.com/tynan"/>
                    <outline title="udinic" text="Geeky adventures...and more..." type="rss" xmlUrl="http://udinic.wordpress.com/feed/"/>
                    <outline title="What If?" text="Answering your hypothetical questions with physics, every Tuesday." type="rss" xmlUrl="http://what-if.xkcd.com/feed.atom"/>
                    <outline title="wtfjs" text="wtfjs" type="rss" xmlUrl="http://wtfjs.com/rss/"/>
                    <outline title="xkcd" text="The blag of the webcomic" type="rss" xmlUrl="http://blag.xkcd.com/feed/"/>
                    <outline title="Zovirl Industries" text="Mark Ivey's blog" type="rss" xmlUrl="http://zovirl.com/feed/"/>
                    <outline title="‫בצק אלים‬" text="‫רשימות ממטבחה של מעצבת‬" type="rss" xmlUrl="http://feeds.feedburner.com/bazekalim"/>
                </outline>
            </body>
        </opml>
        "#;

        let (categories, feeds, mappings) = opml::parse_opml(opml_string, false).unwrap();

        assert_eq!(categories.len(), 1);
        assert_eq!(feeds.len(), 106);
        assert_eq!(mappings.len(), feeds.len());
        assert_eq!(categories.get(0).unwrap().label, "Subscriptions");
        assert_eq!(feeds.get(8).unwrap().label, "AVC");
    }
}
