mod error;

use self::error::{FavIconError, FavIconErrorKind};
use base64;
use chrono::{Duration, NaiveDateTime, Utc};
use failure::ResultExt;
use image::{self, ImageFormat};
use mime_guess;
use models::{Feed, FeedID, Url};
use reqwest;
use serde_json;
use std::collections::hash_map::HashMap;
use std::fs::File;
use std::io::Read;
use std::io::Write;
use std::path::PathBuf;
use webicon::IconScraper;

static FAVICON_META_EXTENSION: &'static str = "icometa";
static FAVICON_EXTENSION: &'static str = "ico";
static EXPIRES_AFTER_DAYS: i64 = 30;

type FavIconResult<T> = Result<T, FavIconError>;

pub struct FavIcons {
    map: HashMap<FeedID, FavIcon>,
    dir: PathBuf,
    client: reqwest::Client,
}

impl FavIcons {
    pub fn new(icon_dir: &PathBuf) -> Self {
        let hash_map: HashMap<FeedID, FavIcon> = HashMap::new();

        FavIcons {
            map: hash_map,
            dir: icon_dir.clone(),
            client: reqwest::Client::new(),
        }
    }

    pub fn get_icon_info(&mut self, feed: &Feed) -> FavIconResult<FavIcon> {
        let favicon = match self.map.get(&feed.feed_id) {
            Some(favicon) => Some(favicon.clone()),
            None => None,
        };

        if let Some(favicon) = favicon {
            if favicon.is_expired() {
                self.map.remove(&feed.feed_id);
                return self.get_icon_info(feed);
            }
            return Ok(favicon);
        }

        if let Ok(favicon) = FavIcon::new_for_feed(feed, &self.dir, &self.client) {
            self.map.insert(feed.feed_id.clone(), favicon.clone());
            return Ok(favicon);
        }

        Err(FavIconErrorKind::NoIcon)?
    }
}

#[derive(Debug, Serialize, Deserialize)]
struct FavIconMeta {
    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub etag: Option<String>,
    pub extension: String,
    pub expires: i64,
}

impl FavIconMeta {
    pub fn load(id: &FeedID, icon_dir: &PathBuf) -> FavIconResult<FavIconMeta> {
        let meta_path = FavIcon::gen_path(&id, icon_dir, FAVICON_META_EXTENSION);
        let mut meta_file = File::open(&meta_path).context(FavIconErrorKind::IO)?;
        let mut meta_content = String::new();
        meta_file
            .read_to_string(&mut meta_content)
            .context(FavIconErrorKind::IO)?;

        let meta: FavIconMeta =
            serde_json::from_str(&meta_content).context(FavIconErrorKind::MetaData)?;
        Ok(meta)
    }

    pub fn is_expired(&self) -> bool {
        Utc::now().naive_local() >= self.expires()
    }

    pub fn expires(&self) -> NaiveDateTime {
        NaiveDateTime::from_timestamp(self.expires, 0)
    }
}

#[derive(Clone, Debug)]
pub struct FavIcon {
    id: FeedID,
    data: Vec<u8>,
    expires: NaiveDateTime,
    format: ImageFormat,
}

impl PartialEq for FavIcon {
    fn eq(&self, other: &FavIcon) -> bool {
        self.id == other.id
    }
}

impl FavIcon {
    pub fn new_for_feed(
        feed: &Feed,
        icon_dir: &PathBuf,
        client: &reqwest::Client,
    ) -> FavIconResult<FavIcon> {
        // try to load the icon from disc
        if let Ok(meta) = FavIconMeta::load(&feed.feed_id, icon_dir) {
            if !meta.is_expired() {
                let icon = Self::load_icon_from_meta(&meta, feed, icon_dir)?;
                return Ok(icon);
            }
        }

        // try to download the icon_url
        if let Some(icon_url) = &feed.icon_url {
            // if meta file is present, check etag of head reqeust
            if let Ok(meta) = FavIconMeta::load(&feed.feed_id, icon_dir) {
                if let Some(file_etag) = &meta.etag {
                    let res = client
                        .head(icon_url.get())
                        .send()
                        .context(FavIconErrorKind::Http)?;
                    if let Some(http_etag) = res.headers().get::<reqwest::header::ETag>() {
                        if file_etag == &http_etag.to_string() {
                            // etag matches, don't download icon again
                            let icon = Self::load_icon_from_meta(&meta, feed, icon_dir)?;
                            return Ok(icon);
                        }
                    }
                }
            }

            // download icon from rss icon_url
            let mut res = client
                .get(icon_url.get())
                .send()
                .context(FavIconErrorKind::Http)?;
            let mut buf: Vec<u8> = Vec::new();
            let mut etag: Option<String> = None;
            if let Some(http_etag) = res.headers().get::<reqwest::header::ETag>() {
                etag = Some(http_etag.to_string());
            }
            res.copy_to(&mut buf).context(FavIconErrorKind::Http)?;
            if let Ok(info) = Self::write_data(
                buf,
                feed,
                &icon_dir,
                etag,
                Self::extension(&icon_url.to_string()),
            ) {
                return Ok(info);
            }
        }

        // try to download a new icon
        if let Some(website) = &feed.website {
            if let Some(info) = Self::scrap_and_download(&website, feed, &icon_dir) {
                return Ok(info);
            }
        } else if let Some(feed_url) = &feed.feed_url {
            if let Some(info) = Self::scrap_and_download(&feed_url, feed, &icon_dir) {
                return Ok(info);
            }
        }

        // FIXME: check if there is a file (without icometa) ... still better than nothing

        Err(FavIconErrorKind::NoIcon)?
    }

    pub fn is_expired(&self) -> bool {
        Utc::now().naive_local() >= self.expires
    }

    fn scrap_and_download(url: &Url, feed: &Feed, icon_dir: &PathBuf) -> Option<FavIcon> {
        let mut scraper = IconScraper::from_http(&url.get().into_string());
        let icons = scraper.fetch_icons();
        let icon = icons.largest();
        if let Some(mut icon) = icon {
            if let Ok(()) = icon.fetch() {
                if let Some(data) = icon.raw {
                    let extension: Option<&str> = match icon.mime_type {
                        Some(mime) => {
                            let primary_type = mime.type_().as_str();
                            let mut sub_type = mime.subtype().as_str().to_owned();
                            if let Some(suffix) = mime.suffix() {
                                sub_type.push_str("+");
                                sub_type.push_str(suffix.as_str());
                            }
                            match mime_guess::get_extensions(primary_type, &sub_type) {
                                Some(extensions) => match extensions.first() {
                                    Some(extension) => Some(extension),
                                    None => None,
                                },
                                None => None,
                            }
                        }
                        None => None,
                    };
                    if let Ok(info) = Self::write_data(data, feed, icon_dir, None, extension) {
                        return Some(info);
                    }
                }
            }
        }
        None
    }

    fn load_icon_from_meta(
        meta: &FavIconMeta,
        feed: &Feed,
        icon_dir: &PathBuf,
    ) -> FavIconResult<FavIcon> {
        let icon_path = Self::gen_path(&feed.feed_id, icon_dir, &meta.extension);
        let mut icon_file = File::open(icon_path).context(FavIconErrorKind::IO)?;
        let mut icon_buffer: Vec<u8> = Vec::new();
        icon_file
            .read_to_end(&mut icon_buffer)
            .context(FavIconErrorKind::IO)?;
        let format = image::guess_format(&icon_buffer).context(FavIconErrorKind::Image)?;

        let icon = FavIcon {
            id: feed.feed_id.clone(),
            data: icon_buffer,
            expires: meta.expires(),
            format: format,
        };

        Ok(icon)
    }

    fn write_data(
        data: Vec<u8>,
        feed: &Feed,
        icon_dir: &PathBuf,
        etag: Option<String>,
        extension: Option<&str>,
    ) -> FavIconResult<FavIcon> {
        let extension = match extension {
            Some(extension) => extension,
            None => FAVICON_EXTENSION,
        };
        let icon_path = Self::gen_path(&feed.feed_id, icon_dir, extension);
        if let Err(err) = File::create(&icon_path) {
            println!("{}", err);
        }
        if let Ok(mut icon_file) = File::create(icon_path) {
            icon_file.write(&data).context(FavIconErrorKind::IO)?;
            let format = image::guess_format(&data).context(FavIconErrorKind::Image)?;
            let expires = Self::gen_expires();

            // gen and write meta
            let meta = FavIconMeta {
                etag: etag,
                extension: extension.to_owned(),
                expires: expires.timestamp(),
            };
            let meta_path = FavIcon::gen_path(&feed.feed_id, icon_dir, FAVICON_META_EXTENSION);
            let meta_string = serde_json::to_string(&meta).context(FavIconErrorKind::MetaData)?;
            let mut meta_file = File::create(meta_path).context(FavIconErrorKind::IO)?;
            meta_file
                .write(&meta_string.as_bytes())
                .context(FavIconErrorKind::IO)?;

            let icon = FavIcon {
                id: feed.feed_id.clone(),
                data: data,
                expires: expires,
                format: format,
            };
            return Ok(icon);
        }
        Err(FavIconErrorKind::IO)?
    }

    fn gen_path(id: &FeedID, icon_dir: &PathBuf, extension: &str) -> PathBuf {
        let id_base64 = base64::encode(id.to_str());
        let mut path = icon_dir.clone();
        path.push(id_base64 + "." + extension);
        path
    }

    fn gen_expires() -> NaiveDateTime {
        Utc::now().naive_utc() + Duration::days(EXPIRES_AFTER_DAYS)
    }

    fn extension(filename: &str) -> Option<&str> {
        filename
            .rfind('.')
            .map(|idx| &filename[idx + 1..])
            .filter(|ext| ext.chars().skip(1).all(|c| c.is_ascii_alphanumeric()))
    }
}

#[cfg(test)]
mod tests {

    use image::ImageFormat;
    use models::{Feed, FeedID, Url};
    use reqwest::Client;
    use std::env;
    use std::fs;
    use std::path::PathBuf;
    use util::favicons::FavIcon;
    use util::feed_parser;

    fn prepare_feed(url_str: &str) -> (Client, PathBuf, Feed) {
        let client = Client::new();
        let icon_dir = env::current_dir().unwrap();
        let icon_dir = icon_dir.join("icons");
        fs::create_dir_all(&icon_dir).unwrap();

        let url = Url::parse(url_str).unwrap();
        let feed_id = FeedID::new(url_str);
        let feed = feed_parser::download_and_parse_feed(&url, &feed_id, None, None).unwrap();

        (client, icon_dir, feed)
    }

    #[test]
    pub fn golem() {
        let (client, icon_dir, golem_feed) =
            prepare_feed("https://rss.golem.de/rss.php?feed=ATOM1.0");
        let info = FavIcon::new_for_feed(&golem_feed, &icon_dir, &client).unwrap();

        assert_eq!(info.id, golem_feed.feed_id);
        assert_eq!(info.format, ImageFormat::ICO);
    }

    #[test]
    pub fn planet_gnome() {
        let (client, icon_dir, gnome_feed) = prepare_feed("http://planet.gnome.org/rss20.xml");
        let info = FavIcon::new_for_feed(&gnome_feed, &icon_dir, &client).unwrap();

        assert_eq!(info.id, gnome_feed.feed_id);
        assert_eq!(info.format, ImageFormat::ICO);
    }

    #[test]
    pub fn reddit_html() {
        let reddit_feed = Feed {
            feed_id: FeedID::new("http://reddit.com"),
            label: String::from("reddit"),
            website: Some(Url::parse("http://reddit.com").unwrap()),
            feed_url: None,
            icon_url: None,
            sort_index: None,
        };

        let (client, icon_dir, _) = prepare_feed("https://rss.golem.de/rss.php?feed=ATOM1.0");
        let info = FavIcon::new_for_feed(&reddit_feed, &icon_dir, &client).unwrap();

        assert_eq!(info.id, reddit_feed.feed_id);
        assert_eq!(info.format, ImageFormat::PNG);
    }
}
