use failure::{Backtrace, Context, Error, Fail};
use std::fmt;

#[derive(Debug)]
pub struct FavIconError {
    inner: Context<FavIconErrorKind>,
}

#[derive(Copy, Clone, Eq, PartialEq, Debug, Fail)]
pub enum FavIconErrorKind {
    #[fail(display = "Failed to parse an Url")]
    Url,
    #[fail(display = "Error performing IO operation")]
    IO,
    #[fail(display = "Error parsing icon metadata file")]
    MetaData,
    #[fail(display = "Http request failed")]
    Http,
    #[fail(display = "No favicon found")]
    NoIcon,
    #[fail(display = "Error parsing image data")]
    Image,
    #[fail(display = "Unknown Error")]
    Unknown,
}

impl Fail for FavIconError {
    fn cause(&self) -> Option<&Fail> {
        self.inner.cause()
    }

    fn backtrace(&self) -> Option<&Backtrace> {
        self.inner.backtrace()
    }
}

impl fmt::Display for FavIconError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Display::fmt(&self.inner, f)
    }
}

impl From<FavIconErrorKind> for FavIconError {
    fn from(kind: FavIconErrorKind) -> FavIconError {
        FavIconError {
            inner: Context::new(kind),
        }
    }
}

impl From<Context<FavIconErrorKind>> for FavIconError {
    fn from(inner: Context<FavIconErrorKind>) -> FavIconError {
        FavIconError { inner: inner }
    }
}

impl From<Error> for FavIconError {
    fn from(_: Error) -> FavIconError {
        FavIconError {
            inner: Context::new(FavIconErrorKind::Unknown),
        }
    }
}
