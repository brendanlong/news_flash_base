CREATE TABLE feeds (
	feed_id TEXT PRIMARY KEY NOT NULL UNIQUE,
	label VARCHAR NOT NULL,
	website TEXT,
	feed_url TEXT,
	icon_url TEXT,
	order_id INTEGER
);

CREATE TABLE categories (
	category_id TEXT PRIMARY KEY NOT NULL UNIQUE,
	label TEXT NOT NULL,
	order_id INTEGER,
	parent_id TEXT NOT NULL
);

CREATE TABLE tags (
	tag_id TEXT PRIMARY KEY NOT NULL UNIQUE,
	label TEXT NOT NULL,
	color INTEGER
);

CREATE TABLE articles (
	article_id TEXT PRIMARY KEY NOT NULL UNIQUE,
	title TEXT,
	author TEXT,
	feed_id TEXT NOT NULL,
	url TEXT,
	timestamp DATETIME NOT NULL,
	html TEXT,
	summary TEXT,
	direction INTEGER,
	unread INTEGER NOT NULL,
	marked INTEGER NOT NULL,
	FOREIGN KEY(feed_id) REFERENCES feeds(feed_id)
);

CREATE TABLE enclosures (
	article_id TEXT PRIMARY KEY NOT NULL,
	url TEXT NOT NULL,
	type INTEGER NOT NULL,
	FOREIGN KEY(article_id) REFERENCES articles(article_id)
);

CREATE TABLE taggings (
	article_id TEXT PRIMARY KEY NOT NULL,
	tag_id TEXT NOT NULL,
	FOREIGN KEY(article_id) REFERENCES articles(article_id),
	FOREIGN KEY(tag_id) REFERENCES tags(tag_id)
);

CREATE TABLE feed_mapping (
	feed_id TEXT PRIMARY KEY NOT NULL,
	category_id TEXT NOT NULL,
	FOREIGN KEY(feed_id) REFERENCES feeds(feed_id),
	FOREIGN KEY(category_id) REFERENCES categories(category_id)
);
